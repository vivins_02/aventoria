const mysql = require('mysql2/promise');

require('dotenv').config();

async function getConnection()
{
  console.log(process.env.DB_HOST);
  
  const poolConnection = await mysql.createPool
  (
    {
      host: process.env.DB_HOST,
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      dateStrings: true,
    }
  );
  return  poolConnection
}

module.exports = { getConnection };
