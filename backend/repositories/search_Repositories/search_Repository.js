const { getConnection } = require('../connectors.js')


const searchQuery = 'SELECT e.*, MAX(f.ruta) as ruta, c.nombre as ciudad FROM experiencias e INNER JOIN `experiencias_ciudades` ec ON e.id=ec.`id_experiencia` INNER JOIN ciudades c ON c.id=ec.`id_ciudad` LEFT JOIN`fotos` f ON e.id = f.id_experiencia WHERE (? IS NULL OR e.nombre LIKE ?) AND (? IS NULL OR `dias_actividad` LIKE ?) AND (? IS NULL OR c.nombre = ?) AND e.precio BETWEEN IF(? IS NOT NULL, ?, "0") AND IF(? IS NOT NULL, ?, "1000.00") GROUP BY e.id  ORDER BY`nombre` ASC' ;


async function searchExperience(req, res) {
    try {
        const { nombre, dias_actividad, ciudad, precioMin, precioMax } = req.query

        console.log(req.query);
        const connection = await getConnection()
        const [getSearchResults] = await connection.query(searchQuery,
            [nombre,
                `%${nombre}%`,
                dias_actividad,
                `%${dias_actividad}%`,
                ciudad,
                ciudad,
                precioMin,
                precioMin,
                precioMax,
                precioMax
            ])

        res.statusCode = 200
        res.send(getSearchResults)

    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 500
        res.send({ error: 'No se pudo realizar la busqueda' })
    }

}

module.exports = { searchExperience }

