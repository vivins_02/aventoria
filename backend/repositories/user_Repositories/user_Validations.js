const joi = require('joi')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const { get } = require('./user_Repository.js')
const { getConnection } = require('../connectors.js')

const getDataUser = 'SELECT * FROM `usuarios` WHERE `email` = ?'


const schemaLogin = joi.object(
    {
        email: joi.string().email().required(),
        password: joi.string().min(5).required()
    })

const schemaUser = joi.object(
    {
        nombre: joi.string().required(),
        apellidos: joi.string().required(),
        email: joi.string().email().required(),
        password: joi.string().min(5).required(),
        telefono: joi.string().required(),
        direccion: joi.string().required(),
        dni: joi.string().required(),
        fecha_nacimiento: joi.date().required(),
        rol: joi.string()
    })

const schemaAdministrator = joi.object(
    {
        rol: joi.string().valid('administrador')
    })

async function isValidLogin(req, res, next) {
    try {
        await schemaLogin.validateAsync(req.body)
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 404
        res.send({ error: 'Usuario o password no cumplen las condiciones'})
    }
}

async function isValidUser(req, res, next) {
    try {
        const user = await get(req.body)

        if (!user) {
            res.statusCode = 404
            res.send({message: 'No existe el usuario'})
        }

        //req.login = { email: user.email, password: user.password }
        req.dataUser = {...user}
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send(error.message)
    }
}

async function isValidUserFormData(req, res, next) {
    try {
        const datosBody = JSON.parse(req.body.data)
        const user = await get(datosBody)

        if (!user) {
            res.statusCode = 404
            res.send({message: 'No existe el usuario'})
        }

        //req.login = { email: user.email, password: user.password }
        req.dataUser = {...user}
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send(error.message)
    }
}

async function isValidPassword(req, res, next) {
    try {
        const isValidPassword = await bcrypt.compare(req.body.password, req.dataUser.password)

        if (!isValidPassword) {
            res.statusCode = 401
            res.send({ error: 'El password no es válido' })
        }
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send(error.message)
    }
}


async function isValidDataUser(req, res, next) {
    try {
        console.log('datos body:',req.body.data);
        const datosBody = JSON.parse(req.body.data)
        console.log(datosBody);
        await schemaUser.validateAsync(datosBody)
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'Alguno de los datos introducidos no es válido'})
    }
}

async function isLogged(req, res, next) {

    try {
        const connection = await getConnection()
        const { authorization } = req.headers

        if (!authorization || !authorization.startsWith('Bearer ')) {
            res.statusCode = 401
            res.send({error: 'Authorization header required'})
        }

        const token = authorization.slice(7, authorization.length)
        const decodedToken = jwt.verify(token, process.env.SECRET)
        const [users] = await connection.query(getDataUser, decodedToken.email)

        if (!users || !users.length) {
            res.statusCode = 401
            res.send({Mensaje: 'El usuario no existe'})
        }

        req.auth = decodedToken
        req.user = users
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send(error.message)
    }
}

async function isAdministrator(req, res, next) {
    try {
        await schemaAdministrator.validateAsync({ rol: req.user[0].rol })
        next()
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'No tienes permisos de administrador'})
    }
}


async function isValidComparePassword(req, res) {
    try {
        const isValidPassword = await bcrypt.compare(req.body.comparePassword, req.dataUser.password)
        if (!isValidPassword) {
            res.statusCode = 401
            res.send({ validPassword: false })
        }
        res.statusCode = 200
        res.send({ validPassword: true })
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send(error.message)
    }
}

module.exports = { isValidLogin, isValidUser, isValidPassword, isValidDataUser, isLogged, isAdministrator, isValidComparePassword, isValidUserFormData }
