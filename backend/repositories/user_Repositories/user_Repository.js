const uuid = require('uuid')
const path = require('path')
const bcrypt = require('bcryptjs')
const { nextDay } = require('date-fns')
const { getConnection } = require('../connectors.js')

const getDataUser = "SELECT * FROM `usuarios` WHERE `email` = ?"
const insertUser = `INSERT INTO usuarios
                    (nombre, apellidos, email, password, telefono, direccion, dni, fecha_nacimiento, rol)
                    VALUES(?,?,?,?,?,?,?,?, 'registrado')`
const updateUser = `UPDATE usuarios SET
                    nombre = ?, apellidos = ?, password = ?, telefono = ?, direccion = ?, fecha_nacimiento = ?, foto = ?
                    WHERE id = ?`
const updatePassword = `UPDATE usuarios SET password = ? WHERE id = ?`

// async function userPhoto(req, res, route){
//     try {
//         const connection = await getConnection()
//         await connection.query(insertUserPhoto,[route])
//     }
//     catch (error)
//     {
//         console.error(error.message)
//         res.statusCode = 401
//         res.send({error: 'No es posible conectar, inténtelo más tarde'})
//     }
// }

async function get(dataUser) {
    try {
        const connection = await getConnection()

        const [row] = await connection.query(getDataUser, dataUser.email)
        return row[0]
    }
    catch (error) {
        console.error(error.message)
    }

}
async function register(req, res, next) {
    try {
        const connection = await getConnection()
        const datosBody = JSON.parse(req.body.data)
        const { nombre, apellidos, email, password, telefono, direccion, dni, fecha_nacimiento } = datosBody
        const passwordHash = await bcrypt.hash(password, 10);

        const dataUser = [nombre, apellidos, email, passwordHash, telefono, direccion, dni, fecha_nacimiento]

        const getIdUser = await connection.query(insertUser, dataUser)

        if(req.files) {
            const foto = req.files.image

            const route = getIdUser[0].insertId + '_' + uuid.v4() + '.jpg'
            foto.mv(path.join(__dirname + '/../../userPhotos/' + route))
    
            const insertUserPhoto = `UPDATE usuarios SET foto=? WHERE id = ${getIdUser[0].insertId}`
    
            await connection.query(insertUserPhoto, route)
        }

        next()
        // res.statusCode = 200
        // res.send('Usuario creado')


    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 500
        res.send({ error: 'No se pudo crear el usuario' })
    }
}

async function editUser(req, res) {
    try {
        const connection = await getConnection()
        const datosBody = JSON.parse(req.body.data)
        const { nombre, apellidos, password, telefono, direccion, fecha_nacimiento } = datosBody
        let passwordHash = await bcrypt.hash(password, 10)

        if (password === req.user[0].password) {
            passwordHash = password
        }

        let dataUser = []
        if (req.files) {
            const foto = req.files.image

            const route = req.user[0].id + '_' + uuid.v4() + '.jpg'
            foto.mv(path.join(__dirname + '/../../userPhotos/' + route))

            const insertUserPhoto = `UPDATE usuarios SET foto=? WHERE id = ${req.user[0].id}`

            await connection.query(insertUserPhoto, route)
            dataUser = [nombre, apellidos, passwordHash, telefono, direccion, fecha_nacimiento, route, req.user[0].id]
        }
        else {
            dataUser = [nombre, apellidos, passwordHash, telefono, direccion, fecha_nacimiento, datosBody.foto, req.user[0].id]
        }
        await connection.query(updateUser, dataUser)




        let user = await get(datosBody)
        user = { ...user, token: datosBody.token }

        res.statusCode = 200
        res.send(user)
    }
    catch (error) {
        res.statusCode = 500
        res.send({ error: 'No se pudo actualizar los datos del usuario' })
    }
}
async function changePassword(req, res) {
    try {
        const connection = await getConnection()
        const { newPassword } = req.body
        let passwordHash = await bcrypt.hash(newPassword, 10)
        const dataUser = [passwordHash, req.user[0].id]
        await connection.query(updatePassword, dataUser)
        let user = await get(req.body)
        user = { ...user, token: req.body.token }
        res.statusCode = 200
        res.send(user)
    }
    catch (error) {
        res.statusCode = 500
        res.send({ error: 'No se pudo actualizar los datos del usuario' })
    }
}




module.exports = { get, register, editUser, changePassword }