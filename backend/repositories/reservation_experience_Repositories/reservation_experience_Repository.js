const { getConnection } = require('../connectors.js');

const insertIdReservationIdExperience = 'INSERT INTO reservas_experiencias (id_reserva, id_experiencia) VALUES (?, ?)';
const updateReservationIdExperience =
  "UPDATE `reservas_experiencias` SET comentario = ?, valoracion = ?, estado = 'pendiente' WHERE id_reserva = ?";

async function insertReservationExperience(idReservation, idExperience) {
  try {
    const connection = await getConnection();
    await connection.query(insertIdReservationIdExperience, [idReservation, idExperience]);
  } catch (error) {
    console.error(error.message);
    res.statusCode = 500;
    res.send({ error: 'No se pudo insertar los datos' });
  }
}

async function rating(req, res) {
  try {
    console.log(req.body);
    const connection = await getConnection();
    await connection.query(updateReservationIdExperience, [
      req.body.comentario,
      req.body.valoracion,
      +req.body.id_reserva,
    ]);

    res.statusCode = 200;
    res.send({ ok: 'Valoración introducida' });
  } catch (error) {
    console.error(error.message);
    res.statusCode = 500;
    res.send({ error: 'No se pudo introducir la valoración. Por favor, inténtelo mas tarde' });
  }
}

module.exports = { insertReservationExperience, rating };
