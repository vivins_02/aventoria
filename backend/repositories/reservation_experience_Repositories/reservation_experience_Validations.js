const joi = require('joi');

const schemaRating = joi.object({
  valoracion: joi.number().min(1).max(5),
  comentario: joi.string(),
  id_reserva: joi.number(),
});

async function isValidRating(req, res, next) {
  try {
    await schemaRating.validateAsync(req.body);
    next();
  } catch (error) {
    res.statusCode = 422;
    res.send({ error: 'Alguno de los datos introducidos no es válido' });
  }
}

module.exports = { isValidRating };
