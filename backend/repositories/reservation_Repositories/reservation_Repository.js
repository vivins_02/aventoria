const dateFns = require('date-fns');
const { getConnection } = require('../connectors.js');
const {
  insertReservationExperience,
} = require('../../repositories/reservation_experience_Repositories/reservation_experience_Repository.js');
const {
  insertUserReservation,
} = require('../../repositories/user_reservation_Repositories/user_reservation_Repository.js');

const insertReservation =
  'INSERT INTO reservas (fecha_compra, fecha_actividad, precio_total, cantidad_reservada, estado) VALUES (?,?,?,?,?)';
const updateReservation = "UPDATE `reservas` SET estado = 'cancelada' WHERE id = ?";
const listReservation =
  'SELECT * FROM `reservas` r INNER JOIN `usuarios_reservas` ur ON r.id = ur.id_reserva AND ur.id_usuario = ? ';
const getReservations =
  'SELECT e.id as `id_experiencia`, e.titulo, r.`fecha_actividad`, r.`precio_total`, r.`cantidad_reservada`, r.estado, r.id as `id_reserva`, f.ruta as ruta  FROM `usuarios_reservas` ur LEFT JOIN reservas r ON ur.`id_reserva`=r.id INNER JOIN `reservas_experiencias` re ON r.id=re.`id_reserva` INNER JOIN experiencias e ON re.`id_experiencia`=e.id LEFT JOIN (SELECT * FROM fotos GROUP BY `id_experiencia`) f ON e.id=f.`id_experiencia` WHERE ur.`id_usuario`= ?';

async function reservation(req, res) {
  try {
    const connection = await getConnection();

    for (let i = 0; i < req.body.length; i++) {
      const totalPrice = req.body[i].precio * parseInt(req.body[i].cantidad_reservada);
      const getIdReservation = await connection.query(insertReservation, [
        dateFns.format(new Date(), 'yyyy-MM-dd'),
        req.body[i].fecha_actividad,
        totalPrice,
        req.body[i].cantidad_reservada,
        'activa',
      ]);
      await insertReservationExperience(getIdReservation[0].insertId, req.body[i].id);
      await insertUserReservation(parseInt(req.user[0].id), getIdReservation[0].insertId);
    }

    res.statusCode = 200;
    res.send({ ok: 'Reserva realizada con éxito' });
  } catch (error) {
    console.error(error.message);
    res.statusCode = 500;
    res.send({ error: 'No se pudo crear la reserva' });
  }
}

// async function dataReservation(req, res) {
//     try {
//         const connection = await getConnection()
//         const [row] = await connection.query(getReservations, req.params)

//         res.statusCode = 200
//         res.send(row)

//     } catch (error) {
//         console.error(error.message)
//         res.statusCode = 500
//         res.send({error:'Error al consultar las reservas'})
//     }

// }

async function cancelReservation(req, res) {
  try {
    const connection = await getConnection();
    await connection.query(updateReservation, req.body.id);

    res.statusCode = 200;
    res.send({ ok: 'Reserva cancelada' });
  } catch (error) {
    console.error(error.message);
    res.statusCode = 500;
    res.send({ error: 'No se pudo cancelar la reserva, porfavor inténtelo más tarde' });
  }
}

async function viewReservation(req, res) {
  try {
    const connection = await getConnection();
    const [row] = await connection.query(getReservations, req.user[0].id);

    res.statusCode = 200;
    res.send(row);
  } catch (error) {
    console.error(error.message);
    res.statusCode = 500;
    res.send({ error: 'Error al consultar las reservas' });
  }
}

module.exports = { reservation, cancelReservation, viewReservation };
