import { useCart } from "./CartContext";
import { useUser } from "./UserContext";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { cleanCart } from "./helpers/cleanCart";
import { formatDate } from "./helpers/formatDate";
import Paypal from "./paypal.png";

function CartPayment() {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [setError] = useState(false);
  const [dataUser] = useState(useUser());

  const [cart, setCart] = useCart();

  const handleReservation = async () => {
    const cartReservationData = cleanCart(cart);

    const res = await fetch(`http://localhost:3500/api/reservation`, {
      method: "POST",
      body: JSON.stringify(cartReservationData),
      headers: {
        Authorization: "Bearer " + dataUser.token,
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();

    setLoading(false);

    if (res.ok) {
      setCart([]);
      history.push("/confirmation");
    } else {
      setError(data.error || true);
    }

    if (loading) {
      return <h1>Realizando registro...</h1>;
    }
  };

  return (
    <section className="CartPayment">
      <div className="order_details">
        {cart &&
          cart.map((p) => (
            <div key={p.id}>
              <p className="order_details_title">{p.titulo}</p>
              <p>{formatDate(p.fecha_actividad)}</p>
              <p>{p.cantidad_reservada} plazas</p>
              <div className="order_details_total">
                <p>Total</p>
                <p className="order_price">
                  {" "}
                  {p.cantidad_reservada * p.precio} €
                </p>
              </div>
              <hr className="order_details_line"></hr>
            </div>
          ))}
        <div className="order_details_payment">
          <img src={Paypal} alt="Paypal" />
          <button className="aventoria_button" onClick={handleReservation}>
            Confirmar pago
          </button>
        </div>
      </div>
    </section>
  );
}

export default CartPayment;
