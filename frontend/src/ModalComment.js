import "./Modal.css";

function ModalComment({ children, setStatusModal }) {
  return (
    <div
      className="modal_comment_background"
      onClick={() => setStatusModal(false)}
    >
      <div onClick={(e) => e.stopPropagation()}>{children}</div>
    </div>
  );
}

export default ModalComment;
