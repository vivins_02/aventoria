import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import esLocale from "date-fns/locale/es";


function ChangeDate({ setShowDatePicker,selectedDate, setSelectedDate}) {

    function handleDateChange(date) {
        setSelectedDate(date)
        setShowDatePicker(false)
    }
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
            <KeyboardDatePicker
                // margin="normal"
                id="date-picker-dialog"
                // label="Date picker dialog"
                format="dd MMM yyyy"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />
        </MuiPickersUtilsProvider>
    )

}

export default ChangeDate