import { useState } from 'react'
import { useUser, useSuperSetUser } from './UserContext'
import { useHistory } from 'react-router-dom'
import './EditUser.css'

function EditUser() {
    const history = useHistory()
    const [dataUser, setDataUser] = useState(useUser())

    const superSetMe = useSuperSetUser()

    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    const handleSubmit = async e => {
        e.preventDefault()
        setLoading(true)

        const avatar = e.target.avatar.files[0];

        const fd = new FormData();
        fd.append("image", avatar);
        fd.append("data", JSON.stringify(dataUser));


        const res = await fetch('http://localhost:3500/api/editUser', {
            method: 'POST',
            body: fd,
            headers: {
                'Authorization': 'Bearer ' + dataUser.token
            }
        })

        const data = await res.json()
        setLoading(false)
        if (res.ok) {
            superSetMe(data)
            history.push('/')
        } else {
            setError(data.error || true)
        }
        data && console.log('datos EditUser', data.id)

    }

    if (loading) {
        return <h1>Actualizando datos...</h1>
    }

    // me && console.log('ME EditUser', me)
    // if (me) {
    //     console.log('Register', me)
    //     return <Redirect to="/" />
    // }

    const handleInput = e => setDataUser({ ...dataUser, [e.target.name]: e.target.value })


    return (
        <>
            <form className='EditUser' onSubmit={handleSubmit}>
                <h2>Editar mis datos</h2>
                <label className='huecoAvatar'>
                    Foto:
                    <div className="cajaFoto">
                        <img className="avatar" src={`http://localhost:3500/static2/${dataUser.foto}`} alt="Avatar user" />
                        <input name="avatar" type="file" accept="image/*" />
                    </div>
                </label>
                <label>
                    <span>Nombre:</span>
                    <input name="nombre" value={dataUser.nombre} onChange={handleInput} />
                </label>
                <label>
                    Apellidos:
                    <input name="apellidos" value={dataUser.apellidos} onChange={handleInput} />
                </label>
                <label>
                    Email:
                    <input name="email" readOnly value={dataUser.email} onChange={handleInput} />
                </label>
                <label>
                    Teléfono:
                    <input name="telefono" value={dataUser.telefono} onChange={handleInput} />
                </label>
                <label>
                    Dirección:
                    <input name="direccion" value={dataUser.direccion} onChange={handleInput} />
                </label>
                <label>
                    DNI/NIE:
                    <input name="dni" readOnly value={dataUser.dni} onChange={handleInput} />
                </label>
                <label>
                    Fecha de nacimiento:
                    <input name="fecha_nacimiento" value={dataUser.fecha_nacimiento} onChange={handleInput} />
                </label>
                {/*<label>
             Foto:
            <input name="photo" required value={dataUser.foto} onChange={handleInput} />
        </label> */}
                <button className="aventoria_button">Confirmar cambios</button>
                {
                    error &&
                    <div className="error">
                        {error === true ? 'Error al cambiar los datos' : error}
                    </div>
                }
                <button className="aventoria_button_second" onClick={() => history.push('/profile')}>Cancelar</button>

            </form>
        </>
    )
}
export default EditUser