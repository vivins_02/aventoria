function uniquesExperiences (experience){
    if (experience) {
        const setObj = new Set();
    
        const experience_unique = experience.reduce((acc, exp) => {
        if (!setObj.has(exp.nombre)){
            setObj.add(exp.nombre, exp)
            acc.push(exp)
        }
        return acc;
        },[]);
        return experience_unique
    }

}

export default uniquesExperiences