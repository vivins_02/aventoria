export default function monthNumberToName (selectedDate){
    const months=["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Noviembre", "Diciembre"]

    return months[selectedDate.getMonth()]


}