import monthNumberToName from "./monthNumberToName"

export function formatDate(date) {
    if(typeof date === 'string'){
        date = new Date(date)
        
    }
    return `${date.getDate()} de ${monthNumberToName(date)} de ${date.getFullYear()}` 
}