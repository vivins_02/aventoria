export function cleanCart(cart) {
    let cartData = JSON.parse(JSON.stringify(cart))
    let cartAux =JSON.parse(JSON.stringify(cartData.map(c => {
        delete c.comments
        delete c.descripcion
        delete c.dias_actividad
        delete c.direccion
        delete c.duracion
        delete c.estado_experiencia
        delete c.nombre
        delete c.photos
        delete c.plazas_disponibles
        delete c.plazas_ocupadas
        delete c.plazas_totales
        delete c.titulo
        return c
    })))

    return cartAux
}

