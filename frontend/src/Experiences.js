import "date-fns";
import { useContext, useState, useEffect } from "react";
import { useUser } from "./UserContext";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useExperiences, useCities } from "./AventoriaApi";
import DateFnsUtils from "@date-io/date-fns";
import esLocale from "date-fns/locale/es";
import { SelectedDateContext } from "./SelectedDateContext";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Pagination from "@material-ui/lab/Pagination";
import Pin from "./aventoria_pin.png";
import uniquesExperiences from "./helpers/uniquesExperiences.js";
import dayNumberToName from "./helpers/dayNumberToName.js";

import "./Experiences.css";

function Experiences() {
  const query = useLocation().search;
  const history = useHistory();
  const [dataUser] = useState(useUser());
  const experience = useExperiences();
  const [selectedDate, setSelectedDate] = useContext(SelectedDateContext);
  const [experienceResults, setExperienceResults] = useState([]);
  const [nombre, setNombre] = useState("");
  const [ciudad, setCiudad] = useState("");
  const [dias_actividad, setDias_actividad] = useState("");
  const [precioMin, setPrecioMin] = useState("");
  const [precioMax, setPrecioMax] = useState("");

  useEffect(() => {
    const cargarExperiencias = async () => {
      const res = await fetch(
        `http://localhost:3500/api/searchExperience${query}`
      );
      const experiences = await res.json();
      setExperienceResults(experiences);
    };
    cargarExperiencias();
  }, [query]);

  const uniquesExp = uniquesExperiences(experience);

  const cities = useCities();

  const formatter = new Intl.NumberFormat("es-ES", {
    style: "currency",
    currency: "EUR",
    minimumFractionDigits: 0,
  });

  function handleDateChange(date) {
    setSelectedDate(date);
    setDias_actividad(date);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    let newQuery = "experiences?";

    if (nombre && nombre !== "null") {
      newQuery += `nombre=${nombre}&`;
    }

    if (dias_actividad) {
      newQuery += `dias_actividad=${dayNumberToName(dias_actividad)}&`;
    }

    if (ciudad) {
      newQuery += `ciudad=${ciudad}&`;
    }

    if (precioMin) {
      newQuery += `precioMin=${precioMin}&`;
    }

    if (precioMax) {
      newQuery += `precioMax=${precioMax}&`;
    }

    history.push(newQuery);
  };

  return (
    <div className="container_experiences">
      <section className="experiences">
        <div className="central">
          <form className="filter_box" onSubmit={handleSubmit}>
            <label htmlFor="experience">
              <p>Experiencia:</p>
              <div className="input_experience">
                <select
                  defaultValue={null}
                  onChange={(e) => setNombre(e.target.value)}
                >
                  <option value="" hidden>
                    Escoge la experiencia
                  </option>
                  <option value="null">Todas las experiencias </option>
                  {uniquesExp &&
                    uniquesExp.map((e) => (
                      <option key={e.id} value={e.nombre}>
                        {e.nombre}
                      </option>
                    ))}
                </select>
              </div>
            </label>
            <label>
              <p>Fecha:</p>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                <KeyboardDatePicker
                  // margin="normal"
                  id="date-picker-dialog"
                  // label="Date picker dialog"
                  format="dd MMM yyyy"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </MuiPickersUtilsProvider>
            </label>
            <label>
              <p>Ciudad:</p>
              <div className="input_city">
                <select
                  defaultValue={null}
                  onChange={(e) => setCiudad(e.target.value)}
                >
                  <option value="" hidden>
                    Escoge la ciudad
                  </option>
                  <option value="null">Todas las ciudades </option>
                  {cities &&
                    cities.map((e) => (
                      <option key={e.id} value={e.nombre}>
                        {e.nombre}
                      </option>
                    ))}
                </select>
              </div>
            </label>

            <label>
              <p>Precio desde:</p>
              <div className="input_price_min">
                <input
                  id="price_min"
                  name="price_min"
                  placeholder="0"
                  onChange={(e) => setPrecioMin(e.target.value)}
                />
              </div>
            </label>

            <label>
              <p>Precio hasta:</p>
              <div className="input_price_max">
                <input
                  id="city"
                  name="price_max"
                  placeholder="1000"
                  onChange={(e) => setPrecioMax(e.target.value)}
                />
              </div>
            </label>
            <button className="aventoria_button">Buscar</button>
          </form>

          <div className="experience_list">
            <div className="experience_list_title_area">
              <h1 className="h1-experiencias">Nuestras experiencias</h1>
              {dataUser.rol === "administrador" && (
                <Link to="/newexperience">
                  <AddCircleOutlineIcon />
                </Link>
              )}
            </div>
            {experienceResults.length > 0 &&
              experienceResults.map((e) => (
                <Link
                  key={e.id}
                  className="individual_experience"
                  to={`/oneexperience/${e.id}`}
                >
                  <section
                    className="experience_photo"
                    style={{
                      backgroundImage: `url(http://localhost:3500/static/${e.ruta})`,
                    }}
                  ></section>
                  <section className="experience_data">
                    <h2 className="experience_name">{e.titulo}</h2>
                    <div className="city_cancelation">
                      <p className="experience_city">
                        <img src={Pin} alt="Pin" /> {e.ciudad}
                      </p>
                      <p className="experience_cancelation_policy">
                        Cancelación gratuita
                      </p>
                    </div>
                    <p className="experience_description">{e.descripcion}</p>
                    <p className="experience_disponibility">
                      Dias de realización: {e.dias_actividad}
                    </p>
                    <h1 className="experience_data_price">
                      {formatter.format(e.precio)}
                    </h1>
                  </section>
                </Link>
              ))}

            <div>
              <Pagination
              // count={noOfPages}
              // page={page}
              // onChange={(value) => setPage(value)}
              // defaultPage={1}
              />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Experiences;
