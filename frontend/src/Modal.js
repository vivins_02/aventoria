import './Modal.css'

function Modal({ children, setStatus, setModalChange }) {
    return (
        <div className="modal-background" onClick={() => setStatus(false)}>
            <div onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}

export default Modal
