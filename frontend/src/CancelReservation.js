import { useState, useEffect } from "react";
import { useUser } from "./UserContext";
import "./CancelReservation.css";

function CancelReservation({ idreserva, setStatusReservation }) {
  const [error, setError] = useState(false);
  const [dataUser] = useState(useUser());
  const [id] = useState({
    id: idreserva,
  });
  const [cancelConfirmation, setCancelConfirmation] = useState(false);

  const handleWindowClick = (e) => {
    if (!e.target.closest(".CancelReservation")) {
      setStatusReservation(false);
    }
  };

  useEffect(() => {
    window.addEventListener("click", handleWindowClick);
    return () => {
      window.removeEventListener("click", handleWindowClick);
    };
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    const res = await fetch("http://localhost:3500/api/cancelReservation", {
      method: "POST",
      body: JSON.stringify(id),
      headers: {
        Authorization: "Bearer " + dataUser.token,
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    if (res.ok) {
      setCancelConfirmation(true);
    } else {
      setError(data.error || true);
    }
  };

  return (
    <section className="CancelReservation">
      {cancelConfirmation === false ? (
        <>
          <form onSubmit={handleSubmit}>
            <p>
              Estás a punto de cancelar tu reserva, ¿estás seguro de continuar?
            </p>
            <div className="cancel_button_area">
              <button
                className="aventoria_button_second"
                onClick={() => setStatusReservation(false)}
              >
                Cancelar
              </button>
              <button className="aventoria_button">Confirmar</button>
            </div>
          </form>

          {error && (
            <div className="error">
              {error === true ? "Error al cambiar los datos" : error}
            </div>
          )}
        </>
      ) : (
        <p>Reserva cancelada.</p>
      )}
    </section>
  );
}

export default CancelReservation;
