import { NavLink, Route, Switch } from "react-router-dom";
import CartCheckYourOrder from "./CartCheckYourOrder";
import CartPersonalData from "./CartPersonalData";
import CartPayment from "./CartPayment";
import "./CartList.css";

function CartList() {
  return (
    <section className="CartList">
      <header className="cart_progress_bar">
        <nav>
          <NavLink
            className="nav_cart_progress"
            activeClassName="active"
            to="/cart"
            exact
          >
            <span className="cart_progress_number">1</span>
            Revisa tu pedido
          </NavLink>

          <NavLink
            className="nav_cart_progress"
            activeClassName="active"
            to="/cart/datospersonales"
            exact
          >
            <span className="cart_progress_number">2</span>
            Datos personales
          </NavLink>

          <NavLink
            className="nav_cart_progress"
            activeClassName="active"
            to="/cart/metododepago"
            exact
          >
            <span className="cart_progress_number">3</span>
            Método de pago
          </NavLink>
        </nav>
      </header>

      <Switch>
        <Route path="/cart" exact>
          <CartCheckYourOrder />
        </Route>
        <Route path="/cart/datospersonales">
          <CartPersonalData />
        </Route>
        <Route path="/cart/metododepago">
          <CartPayment />
        </Route>
      </Switch>
    </section>
  );
}

export default CartList;
