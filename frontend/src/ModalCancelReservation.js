import "./Modal.css";

function ModalCancelReservation({ children, setStatusModalReservation }) {
  return (
    <div
      className="modal_comment_background"
      onClick={() => setStatusModalReservation(false)}
    >
      <div onClick={(e) => e.stopPropagation()}>{children}</div>
    </div>
  );
}

export default ModalCancelReservation;