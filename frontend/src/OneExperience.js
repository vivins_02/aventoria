import { useGetExperience } from "./AventoriaApi";
import { useParams, useHistory } from "react-router-dom";
import { useState, useContext, useEffect } from "react";
import { HashLink as Link } from "react-router-hash-link";
import { SelectedDateContext } from "./SelectedDateContext";
import { useCart } from "./CartContext";
import { useUser } from "./UserContext";
import Carousel from "react-material-ui-carousel";
import Rating from "@material-ui/lab/Rating";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";
import AccessAlarmOutlinedIcon from "@material-ui/icons/AccessAlarmOutlined";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import ChangeDate from "./ChangeDate";
import monthNumberToName from "./helpers/monthNumberToName";
import "./OneExperience.css";

function OneExperience({ setModalCart }) {
  const me = useUser();
  const history = useHistory();
  const { id_experiencia } = useParams();
  const experience = useGetExperience(id_experiencia);

  const [selectedDate, setSelectedDate] = useContext(SelectedDateContext);

  const formatter = new Intl.NumberFormat("es-ES", {
    style: "currency",
    currency: "EUR",
    minimumFractionDigits: 0,
  });

  const [showDatePicker, setShowDatePicker] = useState(false);

  const [expand, setExpand] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const handlePlus = () =>
    quantity < experience.plazas_disponibles && setQuantity(quantity + 1);
  const handleMinus = () => quantity > 1 && setQuantity(quantity - 1);

  const [showConfirmation, setShowConfirmation] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowConfirmation(false);
    }, 5000);
    return () => clearTimeout(timer);
  }, [showConfirmation]);

  const [cart, setCart] = useCart();

  const handleConfirmarReserva = (experience) => {
    setShowConfirmation(true);
    setCart([
      ...cart,
      {
        ...experience,
        cantidad_reservada: quantity,
        fecha_actividad: selectedDate.toISOString().split("T")[0],
      },
    ]);

    setModalCart(true);
    if (cart.some((q) => q.id === experience.id)) {
      setCart(
        cart.map((q) =>
          q.id === experience.id
            ? { ...q, cantidad_reservada: quantity + quantity }
            : q
        )
      );
    } else {
      setCart([
        ...cart,
        {
          ...experience,
          cantidad_reservada: quantity,
          fecha_actividad: selectedDate.toISOString().split("T")[0],
        },
      ]);
    }
  };

  const handleGoToCart = (c) => {
    if (me.length > 0) {
      setModalCart(false);
      history.push("/cart");
    } else {
      history.push("/signin");
    }
  };

  return (
    experience && (
      <>
        <div className="OneExperience">
          <section className="experience_top">
            {/* <div className="experience_title_area"> */}
            <h1 className="experience_title">{experience.titulo}</h1>
            <ul className="experience_details">
              <li>
                <ShoppingBasketOutlinedIcon
                  style={{ fontSize: "28px", color: "#EA6E0E" }}
                />
                <p>{formatter.format(experience.precio)}</p>
              </li>
              <li>
                <AccessAlarmOutlinedIcon
                  style={{ fontSize: "28px", color: "#EA6E0E" }}
                />
                <p>{experience.duracion}</p>
              </li>
              <li>
                <p>
                  <span className="experience_details_highlight">
                    {experience.plazas_totales}
                  </span>{" "}
                  plazas disponibles
                </p>
              </li>
            </ul>
            {/* </div> */}
            <div className="experience_price_area">
              <h2 className="experience_price">
                {formatter.format(experience.precio)}
              </h2>
              <Link to={`/oneexperience/${id_experiencia}/#reservation_area`}>
                <button className="aventoria_button">Reservar</button>
              </Link>
            </div>
          </section>
          <section className="carousel">
            <Carousel
              navButtonsProps={{
                style: {
                  backgroundColor: "gray",
                  borderRadius: 0,
                },
              }}
              NextIcon={<NavigateNextIcon />}
              PrevIcon={<NavigateBeforeIcon />}
            >
              {experience &&
                experience.photos.map((e, i) => (
                  <div
                    className="carousel_div"
                    key={i}
                    style={{
                      backgroundImage: `url(http://localhost:3500/static/${e})`,
                    }}
                  ></div>
                ))}
            </Carousel>
          </section>

          <section className="information_column">
            <div className="information_data_column">
              <article className="information_description">
                <h2>Descripcion</h2>
                <p>{experience.descripcion}</p>
                <p>Dias de realización: {experience.dias_actividad}</p>
              </article>

              <hr></hr>

              <article className="information_details">
                <h2>Detalles</h2>
                <table>
                  <tbody>
                    <tr>
                      <th>Duracción</th>
                      <td>{experience.duracion}</td>
                    </tr>
                    <tr>
                      <th>Idioma</th>
                      <td>Español</td>
                    </tr>
                    <tr>
                      <th>Tipo de bono</th>
                      <td>Eletrónico - Llévalo en tu móvil</td>
                    </tr>
                  </tbody>
                </table>
              </article>

              <hr></hr>

              <article className="information_cancelation">
                <h2>Cancelación gratuita</h2>
                <p>
                  Se aceptarán cancelaciones dentro de las 72h anteriores a la
                  realización de la actividad. Para más información{" "}
                  <Link to="/">pincha AQUI </Link>
                </p>
              </article>

              <hr></hr>

              <article>
                <iframe
                  title="google_map"
                  className="google_map"
                  src={`https://www.google.com/maps/embed/v1/search?q=${experience.direccion}&key=${process.env.REACT_APP_API_GOOGLE_MAPS_KEY}`}
                  allowFullScreen
                  loading="lazy"
                ></iframe>
              </article>

              <hr></hr>

              <article className="information_price">
                <div>
                  <h2 className="price_title">
                    {formatter.format(experience.precio)}
                  </h2>
                  <p>por persona</p>
                </div>
                <Link to={`/oneexperience/${id_experiencia}/#reservation_area`}>
                  <button className="aventoria_button"> Reservar </button>
                </Link>
              </article>

              <hr></hr>
            </div>
            <div className="date_column" id="reservation_area">
              <article className="date_container">
                <div className="selected_date">
                  <p>Fecha seleccionada</p>
                  <p className="date_inherited">
                    {selectedDate.getDate()} de{" "}
                    {monthNumberToName(selectedDate)}
                  </p>
                  <p className="disponibility">
                    <span>{experience.plazas_disponibles} </span>plazas
                    disponibles
                  </p>
                </div>
                {showDatePicker === false ? (
                  <button
                    className="aventoria_button_second_date"
                    onClick={() => {
                      setShowDatePicker(!showDatePicker);
                    }}
                  >
                    Cambiar fecha
                  </button>
                ) : (
                  <ChangeDate
                    className="selected_date_modal"
                    selectedDate={selectedDate}
                    setSelectedDate={setSelectedDate}
                    setShowDatePicker={setShowDatePicker}
                  />
                )}
                <button
                  className="selected_date_button"
                  onClick={() => setExpand(!expand)}
                >
                  Reservar
                </button>
                {expand && (
                  <div className="accordion_quantity_area">
                    <div className="quantity_area">
                      <h4>Plazas a reservar</h4>
                      <div className="spinner">
                        <RemoveCircleOutlineIcon
                          style={{ fontSize: "18px" }}
                          onClick={handleMinus}
                        />
                        <span className="spinner_number">{quantity}</span>
                        <AddCircleOutlineIcon
                          style={{ fontSize: "18px" }}
                          onClick={handlePlus}
                        />
                      </div>
                    </div>
                    <div className="total_area">
                      <h4>Valor total</h4>
                      <p>{quantity * experience.precio} €</p>
                    </div>

                    <button
                      className="aventoria_button_confirm"
                      onClick={() => handleConfirmarReserva(experience)}
                    >
                      Confirmar reserva
                    </button>
                    {showConfirmation && (
                      <div className="box_go_to_cart">
                        <p>Vas a reservar {quantity} plaza/plazas</p>
                        <button
                          className="aventoria_button_second"
                          onClick={handleGoToCart}
                        >
                          Pasar por caja
                        </button>
                      </div>
                    )}
                  </div>
                )}
              </article>
            </div>
          </section>

          <section className="reviews">
            <h2 className="reviews_title">Opiniones de nuestros clientes</h2>
            <p>
              Todas las opiniones han sido escritas por clientes reales que han
              reservado con nosotros.
            </p>

            {experience &&
              experience.comments.map((e, i) => (
                <article className="review_unique" key={i}>
                  <div className="stars_area">
                    <Rating name="read-only" value={e.valoracion} readOnly />
                  </div>
                  <div className="review_area">
                    <p className="review_text">{e.comentario}</p>
                  </div>
                </article>
              ))}
          </section>
        </div>
      </>
    )
  );
}

export default OneExperience;
