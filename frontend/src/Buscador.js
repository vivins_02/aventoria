import 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import esLocale from "date-fns/locale/es"
import { useExperiences } from './AventoriaApi'
import { useContext,useState, } from 'react'
import { useHistory } from 'react-router-dom'
import { SelectedDateContext } from './SelectedDateContext'
import uniquesExperiences from './helpers/uniquesExperiences.js'
import dayNumberToName from './helpers/dayNumberToName.js'

function Buscador() {
    const experience = useExperiences()
    const history = useHistory()
    const [selectedDate, setSelectedDate] = useContext(SelectedDateContext)
    const [nombre, setNombre] = useState("");
    const [ciudad] = useState("");
    const [dias_actividad, setDias_actividad] = useState("");
    const [precioMin] = useState("");
    const [precioMax] = useState("");


    const uniquesExp = uniquesExperiences(experience)

    function handleDateChange(date) {
        setSelectedDate(date);
        setDias_actividad(date);
      }

    const handleSubmit = async (e) => {
        e.preventDefault();
    
        let newQuery = "experiences?";
    
        if (nombre && nombre!=="null" ) {
          newQuery += `nombre=${nombre}&`; 
        }
    
        if (dias_actividad) {
          newQuery += `dias_actividad=${dayNumberToName(dias_actividad)}&`;
        }
    
        if (ciudad) {
          newQuery += `ciudad=${ciudad}&`;
        }
    
        if (precioMin) {
          newQuery += `precioMin=${precioMin}&`;
        }
    
        if (precioMax) {
          newQuery += `precioMax=${precioMax}&`;
        }
    
        history.push(newQuery);
    
      };



    return (
        <div className="portada" style={{ backgroundImage: `url("../portada1.jpg")` }}>
            <select
                defaultValue={null}
                onChange={(e) => setNombre(e.target.value)}>
                <option value="" hidden>Escoge la experiencia</option>
                <option value="null">
                    Todas las experiencias{" "}
                </option>
                {uniquesExp &&
                    uniquesExp.map((e) => (
                    <option key={e.id} value={e.nombre}>
                        {e.nombre}
                    </option>
                ))}
            </select>
            
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                <KeyboardDatePicker
                    // margin="normal"
                    id="date-picker-dialog"
                    // label="Date picker dialog"
                    format="dd MMM yyyy"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
            </MuiPickersUtilsProvider>
            <button className="search_button" onClick={handleSubmit}>Buscar ➤</button>
        </div>
    )
}

export default Buscador;
