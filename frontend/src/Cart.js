import "./Cart.css";
import { useCart } from "./CartContext";
import { useUser } from "./UserContext";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";

function Cart({ setModalCart }) {
  const me = useUser();
  const history = useHistory();

  const [cart, setCart] = useCart();

  const handleEliminate = (p) => {
    setCart(cart.filter((q) => q.id !== p.id));
  };

  const handleGoToCart = (c) => {
    if (me) {
      setModalCart(false);
      history.push("/cart");
    } else {
      history.push("/signin");
    }
  };

  const handleWindowClick = (e) => {
    if (!e.target.closest(".Cart")) {
      setModalCart(false);
    }
  };

  useEffect(() => {
    window.addEventListener("click", handleWindowClick);
    return () => {
      window.removeEventListener("click", handleWindowClick);
    };
  });

  return (
    cart && (
      <section className="Cart">
        {cart.length > 0 ? (
          cart.map((p) => (
            <div className="cart_card" key={p.id}>
              <div
                className="product_photo"
                style={{
                  backgroundImage: `url(http://localhost:3500/static/${p.photos[0]})`,
                }}
              ></div>

              <div className="experience_data_container">
                <button
                  className="experience_eliminate"
                  onClick={() => handleEliminate(p)}
                >
                  X
                </button>
                <p className="experience_cart_name">{p.titulo}</p>
                <p className="experience_cart_price">
                  x {p.cantidad_reservada} ={" "}
                  <span>{p.cantidad_reservada * p.precio} € </span>
                </p>
              </div>
            </div>
          ))
        ) : (
          <p>Tu cesta está vacía. </p>
        )}
        {cart.length > 0 && (
          <>
            <div className="experience_cart_total">
              Total:{" "}
              <span>
                {" "}
                {cart.reduce(
                  (acc, p) => acc + p.cantidad_reservada * p.precio,
                  0
                )}{" "}
                €{" "}
              </span>
            </div>
            <button
              className="aventoria_button_checkout"
              onClick={handleGoToCart}
            >
              Pasar por caja
            </button>
          </>
        )}
      </section>
    )
  );
}

export default Cart;

// onClick={() => setModalCart(false)}
