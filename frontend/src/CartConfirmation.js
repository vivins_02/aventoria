import LogoPic from './logo_pic.png'
import { Link } from "react-router-dom"


function CartConfirmation() {

    return (
        <section className="CartConfirmation">
            <div className="confirmation_container">
                <img src={LogoPic} alt="Logo" /> 
                <h1>Reserva confirmada</h1>
                <Link to="/">Volver al inicio</Link>
            </div>
        </section>
    )
}

export default CartConfirmation