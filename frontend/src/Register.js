import { useState } from "react";
import { useUser, useSuperSetUser } from "./UserContext";
import { Redirect, useHistory } from "react-router-dom";
import "./Register.css";

function Register() {
  const history = useHistory();

  const superSetMe = useSuperSetUser();
  const me = useUser();

  const [user, setUser] = useState({});
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [avatarImage, setAvatarImage] = useState(
    "http://localhost:3500/static2/defaultAvatar.jpeg"
  );
  console.log('fotouser: ', avatarImage);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const avatar = e.target.avatar.files[0];

    const fd = new FormData();
    fd.append("image", avatar);
    fd.append("data", JSON.stringify(user));


    const res = await fetch("http://localhost:3500/api/register", {
      method: "POST",
      body: fd,
    });

    const data = await res.json();
    setLoading(false);
    if (res.ok) {
      superSetMe(data);
    } else {
      setError(data.error || true);
    }
    data && console.log("datos Register", data.id);
  };

  if (loading) {
    return <h1>Realizando registro...</h1>;
  }

  me && console.log("ME Register", me);
  if (me) {
    console.log("Register", me);
    return <Redirect to="/" />;
  }

  return (
    <>
      <form className="register" onSubmit={handleSubmit}>
        <h1 className="h1-register">Registrate en Aventoria</h1>
        <label className="huecoAvatar">
          Foto:
          <div className="cajaFoto">
            <img className="avatar" src={avatarImage} alt="Avatar user" />
            <input
              name="avatar"
              type="file"
              accept="image/*"
              onChange={(e) => {
                const imagen = URL.createObjectURL(e.target.files[0]);
                setAvatarImage(imagen)
              }}
            />
          </div>
        </label>
        <h2>Tus datos de acceso</h2>
        <label>
          Email:
          <input
            name="email"
            placeholder="ejemplo@ejemplo.com"
            value={user.email || ""}
            onChange={(e) => setUser({ ...user, email: e.target.value })}
          />
        </label>
        <label>
          Contraseña:
          <input
            name="password"
            placeholder="Escribe tu contraseña aquí"
            value={user.password || ""}
            onChange={(e) => setUser({ ...user, password: e.target.value })}
            type="password"
          />
        </label>
        {/* <label>
            Confirma tu contraseña:
            <input name="password" placeholder="Escribe tu contraseña aquí" defaultValue={ user.password || '' } onChange={e => setUser({...user, password: e.target.value})} type="password"/>
        </label> */}

        <h2>Tus datos personales</h2>
        <label>
          <span>Nombre:</span>
          <input
            name="nombre"
            placeholder="Escribe tu nombre aquí"
            value={user.nombre || ""}
            onChange={(e) => setUser({ ...user, nombre: e.target.value })}
          />
        </label>
        <label>
          Apellidos:
          <input
            name="apellidos"
            placeholder="Escribe tus apellidos aquí"
            value={user.apellidos || ""}
            onChange={(e) => setUser({ ...user, apellidos: e.target.value })}
          />
        </label>
        <label>
          Teléfono:
          <input
            name="telefono"
            placeholder="666 666 666"
            value={user.telefono || ""}
            onChange={(e) => setUser({ ...user, telefono: e.target.value })}
          />
        </label>
        <label>
          Documento de identificación:
          <input
            name="dni"
            placeholder="DNI, NIE o pasaporte"
            required
            value={user.dni || ""}
            onChange={(e) => setUser({ ...user, dni: e.target.value })}
          />
        </label>
        <label>
          Fecha de nacimiento:
          <input
            name="fecha_nacimiento"
            placeholder="AAAA-MM-DD"
            value={user.fecha_nacimiento || ""}
            onChange={(e) =>
              setUser({ ...user, fecha_nacimiento: e.target.value })
            }
          />
        </label>
        <label>
          Dirección:
          <input
            name="direccion"
            placeholder="Escribe tu dirección aquí"
            value={user.direccion || ""}
            onChange={(e) => setUser({ ...user, direccion: e.target.value })}
          />
        </label>

        {/* <label>
            Foto:
            <input name="photo" type="file" required value={ foto } onChange={e => setFoto(e.target.value)} />
        </label> */}

        <button className="register_button">Registrarse</button>
        {error && (
          <div className="error">
            {error === true ? "Error de registro" : error}
          </div>
        )}
      </form>
      <button onClick={() => history.push("/")} className="register_button_second" >Cancelar</button>
    </>
  );
}
export default Register;
