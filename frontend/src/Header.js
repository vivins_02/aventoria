import "./Header.css";
import Logo from "./logo_aventoria.png";
import { useUser } from "./UserContext";
import { Link } from "react-router-dom";
import { useState } from "react";
import { UserOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import Login from "./Login";
import MenuUser from "./MenuUser";

import { useCart } from "./CartContext";

function Header({ modalCart, setModalCart }) {
  const me = useUser();
  const [cart] = useCart();
  const [status, setStatus] = useState(false);
  const [menuUser, setmenuUser] = useState(false);

  return (
    <header className="Header">
      <section className="central_header">
        <div className="logo">
          <Link to="/">
            <img src={Logo} alt="logo" />
          </Link>
        </div>
        <section className="area">
          <div className="icon_area">
            <div
              className="icon_cart"
              onClick={() => {
                setModalCart(!modalCart);
              }}
            >
              <ShoppingCartOutlined
                style={{ fontSize: "20px", color: "white" }}
              />
              {cart.length > 0 && (
                <div className="cart_notification">
                  <p className="cart_notification_number">{cart.length}</p>
                </div>
              )}
            </div>
            {!me && (
              <span className="icon">
                <UserOutlined
                  style={{ fontSize: "20px", color: "white" }}
                  onClick={() => {
                    setStatus(!status);
                  }}
                />

                {status && <Login setStatus={setStatus} />}
              </span>
            )}
          </div>
          {me && (
            <div className="user-info">
              <div
                className="avatar"
                style={{
                  backgroundImage: `url(http://localhost:3500/static2/${
                    me.foto || process.env.REACT_APP_DEFAULT_AVATAR
                  })`,
                }}
                onClick={() => setmenuUser(!menuUser)}
              />
              {menuUser && <MenuUser setmenuUser={setmenuUser} />}
            </div>
          )}
        </section>
      </section>
    </header>
  );
}

export default Header;
