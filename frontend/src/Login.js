import { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import { useUser, useSuperSetUser } from "./UserContext";
import "./Login.css";

function Login({ status, setStatus }) {
  const [login, setLogin] = useState({});
  const [error, setError] = useState(false);

  const me = useUser();
  const superSetMe = useSuperSetUser();

  const handleWindowClick = (e) => {
    if (!e.target.closest(".Login")) {
      setStatus(false);
    }
  };

  useEffect(() => {
    window.addEventListener("click", handleWindowClick);
    return () => {
      window.removeEventListener("click", handleWindowClick);
    };
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3500/api/login", {
      method: "POST",
      body: JSON.stringify(login),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    if (res.ok) {
      superSetMe(data);
      setStatus(false);
    } else {
      setError(data.error);
    }
    data && console.log("datos", data.id);
  };

  if (me) {
    return <Redirect to="/" />;
  }

  return (
    <section className="Login">
      <h1 className="Title_P1">Mi cuenta</h1>
      <p className="Title_P3">¿Ya tienes cuenta?</p>
      <h2 className="Title_P2">Accede a tu cuenta en Aventoria</h2>
      <form action="" className="form_login" onSubmit={handleSubmit}>
        <label>
          <input
            className="input-login"
            value={login.email || ""}
            onChange={(e) => setLogin({ ...login, email: e.target.value })}
            placeholder="Email"
          />
        </label>
        <label>
          <input
            className="input-login"
            type="password"
            value={login.password || ""}
            onChange={(e) => setLogin({ ...login, password: e.target.value })}
            placeholder="Contraseña"
          />
        </label>

        <button className="aventoria_button">Iniciar sesion</button>
        {error && <p className="error">{error}</p>}
        <p>
          ¿No tienes cuenta?{" "}
          <a
            onClick={() => {
              setStatus(!status);
            }}
            href="/register"
          >
            Registrate aqui
          </a>
        </p>
      </form>
    </section>
  );
}

export default Login;
