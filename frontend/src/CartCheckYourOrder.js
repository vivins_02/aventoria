import { useCart } from "./CartContext";
import { useFiveExperiences } from "./AventoriaApi";
import { useHistory, Link } from "react-router-dom";
import { formatDate } from "./helpers/formatDate";

function CartCheckYourOrder({ modalCart, setModalCart }) {
  const history = useHistory();
  const fiveExperiences = useFiveExperiences();

  const [cart, setCart] = useCart();

  const handleEliminate = (p) => {
    setCart(cart.filter((q) => q.id !== p.id));
  };

  return (
    <section>
      <div className="cart_list_central">
        <div className="cards_area">
          <section className="cards_list">
            {cart.length > 0 &&
              cart.map((p) => (
                <div className="card" key={p.id}>
                  <div
                    className="product_photo"
                    style={{
                      backgroundImage: `url(http://localhost:3500/static/${p.photos[0]})`,
                    }}
                  ></div>
                  <div className="product_data">
                    <button
                      className="product_eliminate"
                      onClick={() => handleEliminate(p)}
                    >
                      X
                    </button>
                    <Link
                      className="link_product_name"
                      to={`/oneexperience/${p.id}`}
                    >
                      <p className="product_name">{p.titulo}</p>
                    </Link>

                    <p className="product_date_selected">
                      {formatDate(p.fecha_actividad)}
                    </p>
                    <div className="product_quantity_price">
                      <p className="product_quantity">
                        <span>{p.cantidad_reservada}</span> plazas
                      </p>
                      <p className="product_price">
                        {p.cantidad_reservada * p.precio} €
                      </p>
                    </div>
                  </div>
                </div>
              ))}
          </section>
          <div className="product_total">
            <div className="product_total_box">
              <p>Precio total</p>
              <p className="product_total_box_price">
                {cart.reduce(
                  (acc, p) => acc + p.cantidad_reservada * p.precio,
                  0
                )}{" "}
                €
              </p>
            </div>
            <div className="product_total_buttons">
              <button
                className="aventoria_button_light"
                onClick={() => history.push("/experiences")}
              >
                Reservar más actividades
              </button>
              <button
                className="aventoria_button_dark"
                onClick={() => history.push("/cart/datospersonales")}
              >
                Tramitar reserva
              </button>
            </div>
          </div>
        </div>

        <section className="cart_related">
          <h3>También te puede interesar</h3>
          <div className="cart_related_cards">
            {fiveExperiences &&
              fiveExperiences.map((f) => (
                <Link
                  key={f.id}
                  className="cajitas"
                  to={`/oneexperience/${f.id}`}
                >
                  <div className="cart_related_photo">
                    <div
                      className="cards_photo"
                      style={{
                        backgroundImage: `url(http://localhost:3500/static/${f.ruta})`,
                      }}
                    ></div>
                    <p className="cards_name">{f.titulo}</p>
                  </div>
                </Link>
              ))}
          </div>
          <button
            className="aventoria_button"
            onClick={() => history.push("/experiences")}
          >
            Ver todas las actividades
          </button>
        </section>
      </div>
    </section>
  );
}

export default CartCheckYourOrder;

// p.fecha_actividad.toISOString().split('T')[0]
