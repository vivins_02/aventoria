import { useState } from "react";
import { useUser } from "./UserContext";
import { useHistory } from "react-router-dom";
import "./NewExperience.css";

function NewExperience() {
  const history = useHistory();
  const [error, setError] = useState(false);

  const [newExperience, setNewExperience] = useState({
    nombre: "",
    titulo: "",
    descripcion: "",
    duracion: "",
    direccion: "",
    precio: "",
    plazas_totales: "",
    dias_actividad: "",
    ciudad: "",
  });

  const [loading, setLoading] = useState(false);
  const [dataUser] = useState(useUser());

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const fd = new FormData();

    for (let i = 0; i < e.target.photos.files.length; i++) {
      fd.append("image", e.target.photos.files[i]);
    }

    fd.append("data", JSON.stringify(newExperience));

    const res = await fetch("http://localhost:3500/api/createExperience", {
      method: "POST",
      body: fd,
      headers: {
        Authorization: "Bearer " + dataUser.token,
      },
    });

    const data = await res.json();
    setLoading(false);
    if (res.ok) {
      history.push(`/oneexperience/${data}`);
    } else {
      setError(data.error || true);
    }
  };

  if (loading) {
    return <h1>Creando experiencia...</h1>;
  }

  const updateDiasActividad = (e) => {
    if (
      e.target.checked &&
      !newExperience.dias_actividad.includes(e.target.value)
    ) {
      setNewExperience({
        ...newExperience,
        dias_actividad: newExperience.dias_actividad + e.target.value,
      });
    } else if (
      !e.target.checked &&
      newExperience.dias_actividad.includes(e.target.value)
    ) {
      setNewExperience({
        ...newExperience,
        dias_actividad: newExperience.dias_actividad.replace(
          e.target.value,
          ""
        ),
      });
    }
  };

  console.log(newExperience);
  return (
    <section className="NewExperience">
      <h1>Crear nueva experiencia</h1>
      <form className="new_experience_form" onSubmit={handleSubmit}>
        <label>
          Nombre
          <input
            name="nombre"
            placeholder="Nombre de la experiencia"
            className="input_text"
            value={newExperience.nombre}
            onChange={(e) => {
              setNewExperience({ ...newExperience, nombre: e.target.value });
            }}
          />
        </label>

        <label>
          Titulo
          <input
            name="title"
            placeholder="Titulo de la experiencia"
            className="input_text"
            value={newExperience.titulo}
            onChange={(e) =>
              setNewExperience({ ...newExperience, titulo: e.target.value })
            }
          />
        </label>

        <label>
          Descripción
          <textarea
            name="description"
            placeholder="Descripción de la experiencia"
            className="input_text_area"
            value={newExperience.descripcion}
            onChange={(e) =>
              setNewExperience({
                ...newExperience,
                descripcion: e.target.value,
              })
            }
          />
        </label>

        <label>
          Duración
          <input
            name="duration"
            placeholder="X horas"
            className="input_text"
            value={newExperience.duracion}
            onChange={(e) =>
              setNewExperience({ ...newExperience, duracion: e.target.value })
            }
          />
        </label>

        <label>
          Dirección
          <input
            name="location"
            placeholder="Dirección de la experiencia"
            className="input_text"
            value={newExperience.direccion}
            onChange={(e) =>
              setNewExperience({ ...newExperience, direccion: e.target.value })
            }
          />
        </label>

        <label>
          Valor
          <input
            name="price"
            placeholder="00.00"
            className="input_text"
            value={newExperience.precio}
            onChange={(e) =>
              setNewExperience({ ...newExperience, precio: e.target.value })
            }
          />
        </label>

        <label>
          Plazas totales
          <input
            name="disponibility"
            placeholder="X"
            className="input_text"
            value={newExperience.plazas_totales}
            onChange={(e) =>
              setNewExperience({
                ...newExperience,
                plazas_totales: e.target.value,
              })
            }
          />
        </label>

        <label>Dias actividad</label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="lunes, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Lunes</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="martes, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Martes</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="miercoles, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Miércoles</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="jueves, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Jueves</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="viernes, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Viernes</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="sabado, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Sábado</span>
        </label>

        <label className="checkbox_input">
          <input
            type="checkbox"
            name="activity_days"
            value="domingo, "
            onChange={updateDiasActividad}
          />
          <span className="checkbox_text">Domingo</span>
        </label>

        <label>
          Ciudad
          <input
            name="disponibility"
            placeholder="Ciudad de la experiencia"
            className="input_text"
            value={newExperience.ciudad}
            onChange={(e) =>
              setNewExperience({ ...newExperience, ciudad: e.target.value })
            }
          />
        </label>

        <label>
          Fotos
          <input
            name="photos"
            type="file"
            multiple
            accept="image/*"
            className="input_text"
          />
        </label>

        <button className="aventoria_button">Crear experiencia</button>
        {error && (
          <div className="error">
            {error === true ? "Error al registrar la experiencia" : error}
          </div>
        )}
      </form>
    </section>
  );
}

export default NewExperience;
