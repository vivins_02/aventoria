import { useState } from "react";
import { useUser } from "./UserContext";
import "./RatingComment.css";
import { Rate } from "antd";

function RatingComment({ idreserva, setStatusModal, statusModal }) {
  const [error, setError] = useState(false);
  const [dataUser] = useState(useUser());
  const [confirmation, setConfirmation] = useState(false);
  const [comment, setComment] = useState({
    comentario: "",
    valoracion: 0,
    id_reserva: idreserva,
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    const res = await fetch("http://localhost:3500/api/rating", {
      method: "POST",
      body: JSON.stringify(comment),
      headers: {
        Authorization: "Bearer " + dataUser.token,
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    if (res.ok) {
      setConfirmation(true);
    } else {
      setError(data.error || true);
    }
  };

  const handleGoToReservation = () => {
    setStatusModal(!statusModal);
  };

  return (
    <section className="RatingComment">
      {confirmation === false ? (
        <form onSubmit={handleSubmit}>
          <textarea
            className="text_comment"
            placeholder="Escribe aquí tu comentario"
            value={comment.comentario}
            onChange={(e) =>
              setComment({ ...comment, comentario: e.target.value })
            }
          />
          <Rate
            value={comment.valoracion}
            onChange={(count) => setComment({ ...comment, valoracion: count })}
          />
          <button className="aventoria_button">Enviar</button>

          {error && (
            <div className="error">
              {error === true ? "Error al registrar la experiencia" : error}
            </div>
          )}
        </form>
      ) : (
        <div className="comment_confirmation">
          <p>Comentário enviado, aguardando aprobación.</p>
          <button className="aventoria_button" onClick={handleGoToReservation}>
            Volver a Reservas
          </button>
        </div>
      )}
    </section>
  );
}

export default RatingComment;
