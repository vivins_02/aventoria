import { useBestExperiences } from "./AventoriaApi";
import { useHistory, Link } from "react-router-dom";

import "./TopExperiences.css";

function TopExperiences() {
  const bestExperience = useBestExperiences();
  const history = useHistory();

  return (
    <section className="top-experiences">
      <h1 className="h1-portada">Experiencias destacadas</h1>
      <div className="central">
        {bestExperience &&
          bestExperience.map((b) => (
            <Link
              key={b.id_experiencia}
              className="cajitas"
              to={`/oneexperience/${b.id_experiencia}`}
            >
              <div className="cajitas">
                <img
                  src={`http://localhost:3500/static/${b.ruta}`}
                  alt="foto-experiencia"
                />
                <p className="nombre_experiencia">{b.titulo}</p>
              </div>
            </Link>
          ))}
      </div>
      <button className="verMas" onClick={() => history.push("/experiences")}>
        Ver más
      </button>
    </section>
  );
}

export default TopExperiences;
