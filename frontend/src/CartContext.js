import { createContext, useContext, useState } from "react";

const CartContext = createContext();

export function CartProvider({ children }) {
  const [cart, setCart] = useState(
    JSON.parse(sessionStorage.getItem("cart")) || []
  );

  const superSetCart = (value) => {
    setCart(value);
    sessionStorage.setItem("cart", JSON.stringify(value || ""));
  };
  return (
    <CartContext.Provider value={[cart, superSetCart]}>
      {children}
    </CartContext.Provider>
  );
}

export function useCart() {
  return useContext(CartContext);
}

export const useSuperSetCart = () => {
  const [, superSetCart] = useContext(CartContext);
  return superSetCart;
};
