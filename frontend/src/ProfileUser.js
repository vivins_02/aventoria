import { Link } from "react-router-dom";
import { useUser } from "./UserContext";
import "./ProfileUser.css";

function ProfileUser() {
  const dataUser = useUser();

  return (
    <form className="profile">
      <h2 className="h2-profile">Mi perfil</h2>
      <label className="huecoAvatar">
        <div className="cajaFoto">
          <img
            className="avatar"
            src={`http://localhost:3500/static2/${dataUser.foto}`}
            alt="Avatar user"
          />
        </div>
      </label>
      <label>
        <span>Nombre:</span>
        <input name="nombre" readOnly value={dataUser.nombre} />
      </label>
      <label>
        Apellidos:
        <input name="apellidos" readOnly value={dataUser.apellidos} />
      </label>
      <label>
        Email:
        <input name="email" readOnly value={dataUser.email} />
      </label>
      <label>
        Teléfono:
        <input name="telefono" readOnly value={dataUser.telefono} />
      </label>
      <label>
        Dirección:
        <input name="direccion" readOnly value={dataUser.direccion} />
      </label>
      <label>
        DNI/NIE:
        <input name="dni" readOnly value={dataUser.dni} />
      </label>
      <label>
        Fecha de nacimiento:
        <input
          name="fecha_nacimiento"
          readOnly
          value={dataUser.fecha_nacimiento}
        />
      </label>
      <Link to="/profile/editUser">
        <button className="button_modify">Modificar mis datos</button>
      </Link>
    </form>
  );
}

export default ProfileUser;
