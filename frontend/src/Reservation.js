import { useState } from "react";
import { Link } from "react-router-dom";
import ModalComment from "./ModalComment";
import RatingComment from "./RatingComment";
import CancelReservation from "./CancelReservation";
import { formatDate } from "./helpers/formatDate";

import "./Reservation.css";

function Reservation({ reservation, idreserva, setIdReserva, estado }) {
  const [statusModal, setStatusModal] = useState(false);
  const [statusReservation, setStatusReservation] = useState(false);

  return (
    <section className="Reservation">
      {reservation.length > 0 ? (
        reservation.map((r, i) => (
          <div
            key={r.id}
            className="card_reservation"
            onClick={() => {
              setIdReserva(r.id_reserva);
            }}
          >
            <div
              className={r.estado === "cancelada" ? "enable" : "disable"}
            ></div>
            <img
              className="card_reservation_img"
              src={`http://localhost:3500/static/${r.ruta}`}
              alt="Experience"
            />
            <div className="card_reservation_data">
              <Link to={`/oneexperience/${r.id_experiencia}`}>
                <h3 className="card_reservation_title">{r.titulo}</h3>
              </Link>
              <p>{formatDate(r.fecha_actividad)}</p>
              <h4 className="card_reservation_price">{r.precio_total} €</h4>

              <p>Plazas reservadas: {r.cantidad_reservada}</p>

              {new Date() >
              new Date(
                parseInt(r.fecha_actividad.split("-")[0]),
                parseInt(r.fecha_actividad.split("-")[1] - 1),
                parseInt(r.fecha_actividad.split("-")[2])
              ) ? (
                <button
                  className="aventoria_button"
                  onClick={() => {
                    setStatusModal(!statusModal);
                  }}
                >
                  Comentar
                </button>
              ) : (
                <button
                  className="aventoria_button"
                  onClick={() => {
                    setStatusReservation(!statusReservation);
                  }}
                >
                  Cancelar Reserva
                </button>
              )}
              {statusModal && (
                <ModalComment setStatusModal={setStatusModal}>
                  <RatingComment
                    idreserva={idreserva}
                    setStatusModal={setStatusModal}
                    statusModal={statusModal}
                  />
                </ModalComment>
              )}

              {statusReservation && (
                <CancelReservation
                  idreserva={idreserva}
                  setStatusReservation={setStatusReservation}
                />
              )}
            </div>
          </div>
        ))
      ) : (
        <div className="card_reservation">
          <div className="card_reservation_data">
            <p className="no_reservation">No tienes reservas.</p>
          </div>
        </div>
      )}
    </section>
  );
}

export default Reservation;
