import React, { useContext, useState } from 'react'

const UserContext = React.createContext();

const UserProvider = ({ children }) => {
    const [me, setMe] = useState(JSON.parse(localStorage.getItem('session')))
    const superSetMe = (value) => {
        setMe(value)
        localStorage.setItem('session', JSON.stringify(value || ''))
    }

    return (
        <UserContext.Provider value={{ me, superSetMe }}>
            {children}
        </UserContext.Provider>
    )
}

export const useUser = () => {
    return useContext(UserContext).me
}

export const useSetUser = () => {
    return useContext(UserContext).setMe
}
export const useSuperSetUser = () => {
    return useContext(UserContext).superSetMe
}

export default UserProvider