import { useState } from 'react'
import { useUser } from './UserContext'
import { useHistory } from "react-router-dom";

function CartPersonalData() {
    const history = useHistory();

    const [dataUser] = useState(useUser())

    return(
        <section className="CartPersonalData">
            <h1>Verificación de datos personales</h1>

            <form className="personal_data_form">
                <label>
                    <span>Correo Eletrónico</span>
                    <input name="email" value={dataUser.email} readOnly/>
                </label>

                <label>
                    <span>Nombre</span>
                    <input name="nombre" value={dataUser.nombre} readOnly/>
                </label>

                <label>
                    <span>Apellidos</span>
                    <input name="apellidos" value={dataUser.apellidos} readOnly/>
                </label>

                <label>
                    <span>Dirección</span>
                    <input name="direccion" value={dataUser.direccion} readOnly/>
                </label>

                <label>
                    <span>Documento de identificación</span>
                    <input name="dni" value={dataUser.dni} readOnly/>
                </label>
            </form>
            <div className="personal_data_buttons">
                <button className="aventoria_button_second" onClick={() => history.push("/profile/editUser")}>Modificar datos</button>
                <button className="aventoria_button" onClick={() => history.push("/cart/metododepago")}>Continuar a la forma de pago</button>
            </div>
        </section>
    )
}

export default CartPersonalData