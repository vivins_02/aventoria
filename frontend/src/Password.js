import { useState } from 'react'
import { useUser, useSuperSetUser } from './UserContext'
import { useHistory } from 'react-router-dom'
import './Password.css'
function Password() {
    const history = useHistory()
    const [dataUser] = useState(useUser())
    const [passOk, setPassOk] = useState(false)
    const [comparePass, setComparePass] = useState()
    const [newPass, setNewPass] = useState({ newPassword: '', confirmPassword: '' })
    const superSetMe = useSuperSetUser()
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)
    const handleVerify = async e => {
        e.preventDefault()
        setLoading(true)
        const res = await fetch('http://localhost:3500/api/comparePassword', {
            method: 'POST',
            body: JSON.stringify({ ...dataUser, comparePassword: comparePass }),
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'Bearer ' + dataUser.token
            }
        })
        const data = await res.json()
        setLoading(false)
        if (data.validPassword) {
            setPassOk(true)
        } else {
            setPassOk(false)
        }
    }
    const handleSubmit = async e => {
        e.preventDefault()
        setLoading(true)
        const res = await fetch('http://localhost:3500/api/changePassword', {
            method: 'POST',
            body: JSON.stringify({ ...dataUser, newPassword: newPass.newPassword }),
            headers: {
                'Content-Type': 'application/json',
                'authorization': 'Bearer ' + dataUser.token
            }
        })
        const data = await res.json()
        setLoading(false)
        if (res.ok) {
            superSetMe(data)
            history.push('/profile')
        } else {
            setError(data.error || true)
        }
        data && console.log('datos EditUser', data.id)
    }
    if (loading) {
        return <h1>Actualizando datos...</h1>
    }
    const handleInput = e => {
        setNewPass({ ...newPass, [e.target.name]: e.target.value })
    }
    if (!passOk) {
        return (
            <form className='changePassword' onSubmit={handleVerify}>
                <h2>Cambiar contraseña</h2>
                <label>
                    Contraseña actual:
                    <input name="comparePassword" placeholder="Escribe tu actual contraseña aquí" value={comparePass || ""} onChange={(e) => setComparePass(e.target.value)} type="password" />
                </label>
                <button className="aventoria_button">Cambiar la contraseña</button>
            </form>
        )
    }
    else {
        return (
            <form className='changePassword' onSubmit={handleSubmit}>
                <h2>Cambiar contraseña</h2>
                <label>
                    Nueva contraseña:
                    <input name="newPassword" value={newPass.newPassword || ''} onChange={handleInput} type="password" />
                </label>
                <label>
                    Confirma la nueva contraseña:
                    <input name="confirmPassword" value={newPass.confirmPassword || ''} onChange={handleInput} type="password" />
                </label>
                {
                    newPass.newPassword === '' && newPass.confirmPassword === ''
                        ? <label>Porfavor inserte una contraseña válida </label>
                        : (!(newPass.newPassword === newPass.confirmPassword) ?
                            <label>Las contraseñas no son iguales</label> : <button className="aventoria_button">Cambiar la contraseña</button>)
                }
                {
                    error &&
                    <div className="error">
                        {error === true ? 'Error al cambiar los datos' : error}
                    </div>
                }
            </form>
        )
    }
}
export default Password