import { Link } from "react-router-dom";
import "./Footer.css";
import {
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
  TrademarkOutlined,
} from "@ant-design/icons";
import "antd/dist/antd.css";

function Footer() {
  return (
    <section className="Footer">
      <div className="footer_first">
        <div className="central_first">
          <ul>
            <li className="link_title">Aventoria</li>
            <Link to="/quienesSomos">
              <li>Quiénes somos</li>
            </Link>
            <Link to="/regalaAventoria">
              <li>Regala Aventoria</li>
            </Link>
          </ul>
          <ul>
            <li className="link_title">Medios de Pago</li>
            <li>Tarjeta de crédito</li>
            <li>Paypal</li>
          </ul>
          <ul>
            <li className="link_title">Ayuda</li>
            <Link to="/contacta">
              <li>Contacta con Aventoria</li>
            </Link>
          </ul>
          <ul>
            <li className="link_title">Síguenos</li>
            <li className="rrss_icon">
              <a
                href="https://es-es.facebook.com/"
                target="_blank"
                rel="noreferrer"
              >
                <FacebookOutlined
                  style={{ fontSize: "18px", color: "white" }}
                />
              </a>
              <a
                href="https://www.instagram.com/?hl=es"
                target="_blank"
                rel="noreferrer"
              >
                <InstagramOutlined
                  style={{ fontSize: "18px", color: "white" }}
                />
              </a>
              <a
                href="https://twitter.com/?lang=es"
                target="_blank"
                rel="noreferrer"
              >
                <TwitterOutlined style={{ fontSize: "18px", color: "white" }} />
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="footer_second">
        <div className="central_second">
          <p>
            <TrademarkOutlined style={{ fontSize: "14px", color: "white" }} />{" "}
            Aventoria 2021
          </p>
          <ul className="second_links">
            <Link to="/">
              <li>Condiciones generales</li>
            </Link>
            <Link to="/">
              <li>Aviso legal</li>
            </Link>
            <Link to="/">
              <li>Política de privacidad</li>
            </Link>
            <Link to="/">
              <li>Cookies</li>
            </Link>
          </ul>
        </div>
      </div>
    </section>
  );
}

export default Footer;
