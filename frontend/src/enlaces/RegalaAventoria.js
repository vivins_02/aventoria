import { Link } from 'react-router-dom'
import './regalaAventoria.css'

function RegalaAventoria(){

    
    return(
    <div className="descripcion">
        <section className="texto">
            <h1>Regala Aventoria</h1>
            <p>¿Tienes que hacer un regalo y no te decides? ¿No sabes si vas a acertar?</p>
            <p>No te precupes más, en aventoria te lo ponemos fácil. Regala uno de nuestros vales. No tienen caducidad y podrá escoger la experiencia que más le guste</p>
            <p>Acertarás seguro</p>
            
        </section>
        <section className="ejemplo">
            <section className="cajaCentral">
                <div className="cajitas">
                    <img className="fotovale" src={`http://localhost:3000/ValeAventoria.png`}  alt="vale" />
                    <p className="nombre">Nuestros vales van desde 50€ hasta 200€</p>
                </div>
               
            </section>
        </section>
        <Link className="contacto" to="/contacta">
                        Ponte en contacto con nosotros para hacer la compra
         </Link>
        <button  className="inicio">
            <Link className="inicio" to="/">
                Volver al inicio
            </Link>
        </button>
     
    </div>
    )
}
export default RegalaAventoria;