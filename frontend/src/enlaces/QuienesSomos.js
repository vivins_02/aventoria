import { Link } from 'react-router-dom'
import './quienesSomos.css'

function QuienesSomos(){

    
    return(
    <div className="datosAventoria">
        <section className="texto">
            <h1>Quienes somos</h1>
            <p>Aventoria surgio como un proyecto final para un grupo de tres ambiciosos estudiantes del bootcamp de HACK A BOSS en Vigo.</p>
            <p>Después de mucho trabajo y dedicación Aventoria se hizo realidad logrando superar las expectactivas presentadas para el desarrollo de la web; demostrando que el trabajo en equipo es fundamental para el desarrollo de una gran idea.</p>
            <p>Actualmente se sitúa en una de las web de referencia para realizar cualquier tipo de aventuras.</p>
        </section>
        <section className="equipo">
            <h1>El equipo</h1>
            <section className="cajaCentral">
                <div className="cajitas">
                    <img className="fotoPerso" src={`http://localhost:3000/Nosotros/CarlosCara.jpeg`}  alt="fotoCarlos" />
                    <p className="nombre">Carlos</p>
                </div>
                <div className="cajitas">
                    <img className="fotoPerso" src={`http://localhost:3000/Nosotros/ViviCara.jpeg`} alt="fotoVivi" />
                    <p>Viviane</p>
                </div>
                    <div className="cajitas">
                    <img className="fotoPerso" src={`http://localhost:3000/Nosotros/ElsaCara.jpeg`} alt="fotoElsa" />
                    <p>Elsa</p>
                </div>
            </section>
        </section>
        <button  className="inicio">
            <Link className="inicio" to="/">
                Volver al inicio
            </Link>
        </button>
     
    </div>
    )
}
export default QuienesSomos;