import { Link } from 'react-router-dom'
import './contacta.css'
import {
    FacebookOutlined,
    InstagramOutlined,
    TwitterOutlined,
  } from "@ant-design/icons";
  import "antd/dist/antd.css";

function Contacta(){

    
    return(
    <div className="descripcion">
        <section className="texto">
            <h1>Nuestros datos de contacto</h1>
            <p>Si tienes cualquier duda puedes contactar con nosotros a traves:</p>
            <p>de nuestro email</p>
            <li>info@aventoria.com</li>
            <p className="rrss">o en nuestras redes sociales</p>
            <li className="rrss_icon">
              <a
                href="https://es-es.facebook.com/"
                target="_blank"
                rel="noreferrer"
              >
                <FacebookOutlined
                  style={{ fontSize: "50px", color: "var(--aventoria-dark)" }}
                />
              </a>
              <a
                href="https://www.instagram.com/?hl=es"
                target="_blank"
                rel="noreferrer"
              >
                <InstagramOutlined
                  style={{ fontSize: "50px", color: "var(--aventoria-dark)" }}
                />
              </a>
              <a
                href="https://twitter.com/?lang=es"
                target="_blank"
                rel="noreferrer"
              >
                <TwitterOutlined style={{ fontSize: "50px", color: "var(--aventoria-dark)" }} />
              </a>
            </li>
        </section>
    
        <button  className="inicio">
            <Link className="inicio" to="/">
                Volver al inicio
            </Link>
        </button>
     
    </div>
    )
}
export default Contacta;