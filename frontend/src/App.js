import "./App.css";
import { Route, Switch } from "react-router-dom";
import { useState } from "react";
import Header from "./Header";
import Login from "./Login";
import Home from "./Home";
import Footer from "./Footer";
import Register from "./Register";
import Experiences from "./Experiences";
import Profile from "./Profile";
import OneExperience from "./OneExperience";
import NewExperience from "./NewExperience";
import Cart from "./Cart";
import CartList from "./CartList";
import CartConfirmation from "./CartConfirmation";
import LoginPage from "./LoginPage";
import QuienesSomos from "./enlaces/QuienesSomos";
import RegalaAventoria from "./enlaces/RegalaAventoria";
import Contacta from "./enlaces/contacta";

function App() {
  const [modalCart, setModalCart] = useState(false);

  return (
    <div className="App">
      <Header modalCart={modalCart} setModalCart={setModalCart} />
      {modalCart && <Cart modalCart={modalCart} setModalCart={setModalCart} />}
      <main>
        <Switch>
          <Route path="/login" exact>
            <Login />
          </Route>
          <Route path="/signin" exact>
            <LoginPage />
          </Route>
          <Route path="/register" exact>
            <Register />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
          <Route path="/experiences" exact>
            <Experiences />
          </Route>
          <Route path="/oneexperience/:id_experiencia" exact>
            <OneExperience modalCart={modalCart} setModalCart={setModalCart} />
          </Route>
          <Route path="/newexperience" exact>
            <NewExperience />
          </Route>
          <Route path="/cart">
            <CartList modalCart={modalCart} setModalCart={setModalCart} />
          </Route>
          <Route path="/confirmation" exact>
            <CartConfirmation />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/quienesSomos" exact>
            <QuienesSomos />
          </Route>
          <Route path="/regalaAventoria" exact>
            <RegalaAventoria />
          </Route>
          <Route path="/contacta" exact>
            <Contacta />
          </Route>
          <Route path="*">
            <section>
              <h1>Página no encontrada</h1>
            </section>
          </Route>
        </Switch>
      </main>

      <Footer />
    </div>
  );
}

export default App;
