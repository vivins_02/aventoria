import React, {  useState } from 'react'

export const SelectedDateContext = React.createContext();

export const SelectedDateProvider = ({ children }) => {
    const [selectedDate, setSelectedDate] = useState(new Date())

    return (
        <SelectedDateContext.Provider value={[selectedDate, setSelectedDate]}>
            {children}
        </SelectedDateContext.Provider>
    )
}