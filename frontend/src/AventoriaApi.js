import useFetch from "./useFetch"


export const useExperiences = () => useFetch('http://localhost:3500/api/viewAllExperiences')
export const useBestExperiences = () => useFetch('http://localhost:3500/api/viewBestExperiences')
export const useFiveExperiences = () => useFetch('http://localhost:3500/api/viewFiveExperiences')
export const useBestCities = () => useFetch('http://localhost:3500/api/viewBestCities')
export const useGetUser = () => useFetch('')
export const useGetPhotos = (id_experiencia) => useFetch(`http://localhost:3500/api/viewExperiencesPhotos/${id_experiencia}`)
export const useGetExperience = (id_experiencia) => useFetch(`http://localhost:3500/api/viewOneExperience/${id_experiencia}`)
export const useCities = () => useFetch('http://localhost:3500/api/viewCities')


