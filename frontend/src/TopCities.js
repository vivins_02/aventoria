import { useBestCities } from "./AventoriaApi";
import { Link } from "react-router-dom";
import "./TopCities.css";

function TopCities() {
  const bestCities = useBestCities();

  return (
    <section className="top-cities">
      <h1 className="h1-portada">Ciudades destacadas</h1>
      <div className="central">
        {bestCities &&
          bestCities.map((b) => (
            <Link
              key={b.id_experiencia}
              className="cajitas"
              to={`/experiences?ciudad=${b.nombre}`}
            >
              <div key={b.id} className="cajitas">
                <img
                  src={`http://localhost:3000/cityImages/${b.id}.jpg`}
                  alt="foto-ciudad"
                />
                <p className="nombre_ciudad">{b.nombre}</p>
              </div>
            </Link>
          ))}
      </div>
    </section>
  );
}

export default TopCities;
