import { NavLink, Route, Switch } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useUser } from "./UserContext";
import { useState } from "react";
import "./Profile.css";
import ProfileUser from "./ProfileUser";
import Password from "./Password";
import EditUser from "./EditUser";
import Reservation from "./Reservation";

function Profile() {
  const history = useHistory();
  const [dataUser] = useState(useUser());
  const [reservation, setReservation] = useState([]);
  const [idreserva, setIdReserva] = useState();
  const [estado] = useState();

  const handleReservation = async () => {
    const res = await fetch("http://localhost:3500/api/viewReservation", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + dataUser.token,
      },
    });

    const dataReservation = await res.json();
    setReservation(dataReservation);
  };

  return (
    <section className="todo">
      <h1 className="h1-profile">Perfil de usuario</h1>
      <div className="secciones">
        <nav className="accordion">
          <NavLink className="nav" activeClassName="active" to="/profile" exact>
            Mi perfil
          </NavLink>
          <NavLink
            className="nav"
            activeClassName="active"
            to="/profile/password"
          >
            Cambiar contraseña
          </NavLink>
          <NavLink
            className="nav"
            activeClassName="active"
            to="/profile/reservation/"
            onClick={handleReservation}
          >
            Ver mis reservas
          </NavLink>
          <button className="profile_button" onClick={() => history.push("/")}>
            Volver a Inicio
          </button>
        </nav>
        <div className="user_form_area">
          <Switch>
            <Route path="/profile" exact>
              <ProfileUser />
            </Route>
            <Route path="/profile/password">
              <Password />
            </Route>
            <Route path="/profile/reservation">
              <Reservation
                estado={estado}
                reservation={reservation}
                setReservation={setReservation}
                idreserva={idreserva}
                setIdReserva={setIdReserva}
              />
            </Route>
            <Route path="/profile/editUser">
              <EditUser />
            </Route>
          </Switch>
        </div>
      </div>
    </section>
  );
}
export default Profile;
