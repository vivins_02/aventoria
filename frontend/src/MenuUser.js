import { useSuperSetUser } from "./UserContext";
import { useSuperSetCart } from "./CartContext";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import "./MenuUser.css";

function MenuUser({ setmenuUser }) {
  const superSetMe = useSuperSetUser();
  const superSetCart = useSuperSetCart();

  const handleClickUnlog = () => {
    superSetCart([]);
    superSetMe();
  };

  const handleClickUser = () => {
    setmenuUser(false);
  };

  useEffect(() => {
    const closeMenu = (e) => {
      setmenuUser(false);
    };

    window.addEventListener("click", closeMenu);

    return () => {
      window.removeEventListener("click", closeMenu);
    };
  }, [setmenuUser]);

  return (
    <section className="MenuUser">
      <ul>
        <Link to="/profile">
          <li onClick={handleClickUser}>Perfil de usuario</li>
        </Link>
        <Link to="/">
          <li onClick={handleClickUnlog}>Salir de la cuenta</li>
        </Link>
      </ul>
    </section>
  );
}

export default MenuUser;
