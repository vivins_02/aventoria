import { useState } from 'react'
import { Redirect } from 'react-router-dom'
import { useUser, useSuperSetUser } from './UserContext'
import './LoginPage.css'


function LoginPage() {
    const [login, setLogin] = useState({})
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    const me = useUser()
    const superSetMe = useSuperSetUser()

    // const closeLogin = (e) => {
    //     setStatus(false);
    // }

    // useEffect(() => {

    //     window.addEventListener('click', closeLogin)

    //     return () => {
    //         window.removeEventListener('click', closeLogin)
    //     }

    // }, [setStatus, closeLogin])


    const handleSubmit = async e => {
        e.preventDefault()
        setLoading(true)
        const res = await fetch('http://localhost:3500/api/login', {
            method: 'POST',
            body: JSON.stringify(login),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        const data = await res.json()
        setLoading(false)
        if (res.ok) {
            superSetMe(data)
        } else {
            setError(data.error)
        }
        data && console.log('datos', data.id)
    }

    if (loading) {
        return <h1>Cargando...</h1>
    }

    if (me) {
        return <Redirect to="/cart" />
    }

    return (
        <section className="LoginPage">
            <div className="login_container">
            <p className="Title_P3">¿Ya tienes cuenta?</p>
            <h2 className="Title_P2">Accede a tu cuenta en Aventoria</h2>
            <form action="" className="form_login" onSubmit={handleSubmit}>
                <label>

                    <input className="input-login"
                        value={login.email || ''}
                        onChange={e => setLogin({ ...login, email: e.target.value })}
                        placeholder="Usuario o email"

                    />
                </label>
                <label>

                    <input className="input-login"
                        type='password' value={login.password || ''}
                        onChange={e => setLogin({ ...login, password: e.target.value })}
                        placeholder="Contraseña"
                    />
                </label>

                <button className="aventoria_button">Iniciar sesion</button>
                {error &&
                    <p className="error">{error}</p>
                }
                <p>¿No tienes cuenta? <a href="/register">Registrate aqui</a></p>
            </form>
            </div>
        </section>
    )
}

export default LoginPage