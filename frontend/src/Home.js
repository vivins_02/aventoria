import "./Home.css";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import Buscador from './Buscador'
import TopExperiences from './TopExperiences'
import TopCities from './TopCities'




function Home() {
  
  return (
    <>
      <Buscador />
      <TopExperiences />
      <TopCities />
    </>
  );
}

export default Home;
