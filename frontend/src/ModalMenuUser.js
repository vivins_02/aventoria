import './Modal.css'

function Modal({ children, setmenuUser }) {
    return (
        <div className="modal-background" onClick={() => setmenuUser(false)}>
            <div onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}

export default Modal
