CREATE DATABASE  IF NOT EXISTS `aventoria` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `aventoria`;
-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: localhost    Database: aventoria
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudades` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES (1,'Oia'),(2,'Gondomar'),(3,'Nigrán'),(4,'Padrón'),(5,'Vigo'),(6,'Ourense'),(7,'A Coruña'),(8,'Baiona'),(9,'Moaña');
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencias`
--

DROP TABLE IF EXISTS `experiencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experiencias` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `duracion` varchar(20) DEFAULT NULL,
  `direccion` varchar(300) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `plazas_totales` tinyint NOT NULL,
  `dias_actividad` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencias`
--

LOCK TABLES `experiencias` WRITE;
/*!40000 ALTER TABLE `experiencias` DISABLE KEYS */;
INSERT INTO `experiencias` VALUES (1,'padelsurf','PadelSurf en Baiona','Si te gusta la playa y el mar, que mejor manera de disfrutarlos que recorrer las playas de Baiona en una tabla de PadelSurf','2 horas','Playa Barbeira, Baiona',30.00,10,'jueves, viernes, sabado, domingo',1),(2,'masaje','Masaje terapéutico en Hotel Talaso Atlántico 4*','Disfrute de un masaje terapéutico con unas vistas increibles a las islas Cíes','1 hora y media','Cabo Silleiro, Oia',90.00,5,'lunes, miercoles, viernes, sabado, domingo',1),(3,'ruta','Ruta a caballo en el Galiñeiro','Aléjese de la ciudad con un buen paseo a caballo recorriendo sendas en el monte Galiñeiro','3 horas','Merendeiro do Galiñeiro, Gondomar',80.00,8,'jueves, viernes, sabado, domingo',1),(4,'surf','Surf en Patos','Sol, playa y olas, lo necesario para poder pasar un rato inolvidable domando al mar y disfrutando de este precioso deporte','1 hora','Playa de Patos, Nigrán',50.00,15,'lunes, miercoles, viernes, domingo',1),(5,'senderismo','Senderismo en el Camino','Disfruta de la llamada última etapa del camino de Santiago recorriendo lugares de encanto','6 horas','Albergue Correidoiras, Padrón',20.00,20,'lunes, miercoles, viernes, sabado, domingo',1),(6,'ruta','Ruta histórica en Vigo','Conoce la historia de Vigo siendo acompañado por grandes conocedores de la misma, disfruta de las representaciones que hacen que todo parezca más real','3 horas','Plaza Princesa, Vigo',50.00,30,'viernes, sabado, domingo',1),(7,'cayak','Cayak por el miño','Desciende por el río Miño disfrutando de los paisajes que esta tierra nos da','4 horas','Termas de Outariz, Ourense',60.00,6,'jueves, viernes, sabado, domingo',1),(8,'bautismo de buceo','Bautismo de buceo en A Coruña','Conoce por primera vez lo qué se esconde debajo de las olas con este Bautismo de Buceo en Coruña. ¡Este es tu momento!','3 horas','playa de las Lapas, A Coruña',75.00,5,'lunes, martes, miércoles, viernes, sabado',1),(9,'puesta de sol en velero','Puesta de sol en velero en las Islas Cíes','Un mar transparente, un cielo despejado y una puesta de sol de fantasía, todo esto a bordo de un velero precioso con las islas Cíes a pocos metros','5 horas','Avenida Arquitecto Jesús Valverde, Baiona',120.00,8,'lunes, miercoles, viernes, sabado, domingo',1),(10,'ruta','Ruta a caballo Chan da Lagoa','Respirar aire puro, mezclarte con la naturaleza y recorrer unos montes preciosos desde la virgen de la roca hasta Chan da Lagoa a lomos de un buen caballo, que mejor plan?','3 horas','Virgen de la Roca, Baiona',65.00,6,'jueves, viernes, sabado, domingo',1),(11,'bautismo de buceo','Bautismo de buceo en las Islas Cíes','Las primeras impresiones dicen que son las más importantes, y que mejor primera impresión que sumergirte por primera vez en las aguas de las islas Cíes!','3 horas','Islas Cíes, Vigo',90.00,3,'miercoles, jueves, viernes, sabado, domingo',1),(12,'spa','Circuito Spa en Hotel Pazo los Escudos 5*','En el centro termal del Hotel Pazo los Escudos tendrá lugar una de las experiencias más relajantes que podáis encontrar. Este spa es sinónimo de paz y tranquilidad. Un lugar en el que vuestro cuerpo recibirá el cuidado que se merece.','1 hora','Pazo los Escudos, Vigo',120.00,4,'lunes, jueves, viernes, sabado, domingo',1),(13,'flyboard','Flyboard Moaña','Si te has decidido por disfrutar de una actividad inolvidable este es el momento perfecto. No te lo pienses dos veces y sobrevuela el mar de una forma fascinante. La aventura empezará con una breve sesión teórica en la que te facilitarán el material y te enseñarán todo lo necesario para realizar la experiencia de una forma segura. Una vez hayas adquirido todos los conocimientos pasarás a la acción.','1 hora','Praia do Puntillón, Moaña',100.00,5,'lunes, miercoles, viernes, sabado',1),(14,'parapente','Vuelo en Parapente en Santa María de Oia','El vuelo se realizará por Santa María de Oia, a unos diez kilómetros de Vigo, y podrás contemplar los acantilados y la costa con el mar de fondo. Descubre qué se siente desde las alturas y disfruta de un paisaje increíble. Liberarás la adrenalina y además te llevarás a casa un vídeo de tu vuelo que te entregarán al momento.','2 horas','Bar Ladeira, Torroña, Oia',140.00,2,'lunes, miercoles, viernes, sabado, domingo',1),(15,'puenting','Puenting en Baiona','¿Has subido alguna vez a una caída libre en una feria? Multiplica esa sensación por veinte y tendrás lo que se siente en el puenting en Galicia.','30 minutos','Parque da Palma, Baiona',50.00,10,'martes, jueves, sabado, domingo',1),(16,'ruta','Ruta en Moto de Agua en Baiona','Disfruta conduciendo tú mismo una moto de agua desde el principio hasta el fin de la excursión y desafía las olas del mar.','1 hora','Puerto deportivo, Baiona',60.00,7,'jueves, viernes, sabado, domingo',1),(17,'ruta','Ruta en Moto de Agua en Vigo','Disfruta conduciendo tú mismo una moto de agua desde el principio hasta el fin de la excursión y desafía las olas del mar.','1 hora','Playa de Samil, Vigo',60.00,7,'miercoles, jueves, viernes, sabado, domingo',1);
/*!40000 ALTER TABLE `experiencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencias_ciudades`
--

DROP TABLE IF EXISTS `experiencias_ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experiencias_ciudades` (
  `id_experiencia` int unsigned NOT NULL,
  `id_ciudad` int unsigned NOT NULL,
  PRIMARY KEY (`id_experiencia`,`id_ciudad`),
  KEY `id_ciudad` (`id_ciudad`),
  CONSTRAINT `experiencias_ciudades_ibfk_1` FOREIGN KEY (`id_experiencia`) REFERENCES `experiencias` (`id`),
  CONSTRAINT `experiencias_ciudades_ibfk_2` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencias_ciudades`
--

LOCK TABLES `experiencias_ciudades` WRITE;
/*!40000 ALTER TABLE `experiencias_ciudades` DISABLE KEYS */;
INSERT INTO `experiencias_ciudades` VALUES (2,1),(14,1),(3,2),(4,3),(5,4),(6,5),(11,5),(12,5),(17,5),(7,6),(8,7),(1,8),(9,8),(10,8),(15,8),(16,8),(13,9);
/*!40000 ALTER TABLE `experiencias_ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fotos` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_experiencia` int unsigned DEFAULT NULL,
  `descripcion` text,
  `ruta` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_experiencia` (`id_experiencia`),
  CONSTRAINT `fotos_ibfk_1` FOREIGN KEY (`id_experiencia`) REFERENCES `experiencias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (1,1,NULL,'1_15b4f76f-9e32-42ad-8d36-3b956ddf0dea.jpg'),(2,1,NULL,'1_0d6a75a8-0b0c-44ed-aa62-c8e848c137f0.jpg'),(3,1,NULL,'1_9bbfbdb0-04fb-434b-8ddf-e7cf5ef6dd1f.jpg'),(4,1,NULL,'1_92a3d1b9-9bf6-480a-9ac8-d150afd8a911.jpg'),(5,2,NULL,'2_7030e71c-c00b-4fb6-be35-82c29dd1f392.jpg'),(6,2,NULL,'2_67a2530c-abe7-42cf-9fbd-a7855f3ac19a.jpg'),(7,2,NULL,'2_058adb3f-eb55-48ad-92ab-b5dbbba69165.jpg'),(8,2,NULL,'2_4096f48c-405f-432d-b6da-3fb663ccf412.jpg'),(9,3,NULL,'3_b45d6500-9400-4fdd-9a66-96a64e8a70b5.jpg'),(10,3,NULL,'3_7968e41e-5050-45c5-9314-5fc185ea48b6.jpg'),(11,3,NULL,'3_7ef4bf40-ee03-4a0f-8f20-5b30bc23c2d5.jpg'),(12,3,NULL,'3_07c32c16-b304-467c-9696-3d1a0847bf5a.jpg'),(13,4,NULL,'4_b6407170-6f76-4951-9e51-90451ab122fd.jpg'),(14,4,NULL,'4_b6ffeb64-9b0b-4635-b62d-aecca3b15886.jpg'),(15,4,NULL,'4_0b021027-ac73-4c07-b47c-0346a12be732.jpg'),(16,4,NULL,'4_96f0f0fb-84c3-475c-98fb-86bdb089c7f0.jpg'),(17,5,NULL,'5_3ebc6ac1-3279-4daf-9e57-0a18d0077f57.jpg'),(18,5,NULL,'5_025f6b07-9a3d-4577-8ed7-0a871e409420.jpg'),(19,5,NULL,'5_12e9394c-2795-4814-b3c2-da64fa0d47c7.jpg'),(20,5,NULL,'5_d38f02df-b75e-486c-b7d8-88b2daae6403.jpg'),(21,6,NULL,'6_9da6db77-911f-4a8f-acb8-f0a7ddb8347f.jpg'),(22,6,NULL,'6_dc12cb53-b9ff-4e2f-8f32-f1f27daf92b8.jpg'),(23,6,NULL,'6_012abda8-7731-4650-8ae2-1c4c472c1ef7.jpg'),(24,6,NULL,'6_2dc73baf-3daa-487c-85c5-0431ee922dcc.jpg'),(25,7,NULL,'7_381a2cb3-6cfc-4fe0-bb2b-0e14234d0112.jpg'),(26,7,NULL,'7_db4a2c72-4700-4e8b-8cde-32743912d5f0.jpg'),(27,7,NULL,'7_839d9857-6270-4018-b130-5d7b86c52809.jpg'),(28,7,NULL,'7_8bae7373-e1e6-4f1f-9ed5-ee0d0241bc3b.jpg'),(29,8,NULL,'8_d1c74115-4083-45cf-8360-c3854f092504.jpg'),(30,8,NULL,'8_595135f4-1f5a-4810-bfce-777902f105c1.jpg'),(31,8,NULL,'8_19b53375-e4b6-40db-ad91-976cd49bfcd9.jpg'),(32,8,NULL,'8_a3118962-b677-4714-9f87-7e75ff79e183.jpg'),(33,9,NULL,'9_b87b09bd-8bb1-45aa-b702-5a058ec97ee2.jpg'),(34,9,NULL,'9_7d6993f0-4c94-4e9e-bd72-4fd09557900b.jpg'),(35,9,NULL,'9_0cdb7199-54a4-47b3-8506-7254123c44dc.jpg'),(36,9,NULL,'9_8fa08aa8-9bd8-408f-a4ed-e5e23e3c46bf.jpg'),(37,10,NULL,'10_fd10097b-e346-4dc3-ab80-282107ca66e8.jpg'),(38,10,NULL,'10_56142bcd-4bc8-4c02-989f-7eb491496b5a.jpg'),(39,10,NULL,'10_987cd301-9243-4e39-b488-26c21bcc398f.jpg'),(40,10,NULL,'10_c4ae517d-6fcc-47b9-8132-3c4d95aaa37f.jpg'),(41,11,NULL,'11_ed38eecf-3817-4574-ab1b-35137f6f73e3.jpg'),(42,11,NULL,'11_e68177e5-5807-41cd-9cb6-9baeb9d2639e.jpg'),(43,11,NULL,'11_968fc6f5-e399-4e4f-9fcc-168157f70534.jpg'),(44,11,NULL,'11_656b0bd3-dcca-4967-9246-1850570ceafe.jpg'),(45,12,NULL,'12_bbaf0b12-7076-4d4b-b487-aded3eabe5c6.jpg'),(46,12,NULL,'12_5a79f4c5-fc4b-4de9-973c-ed8a92136aba.jpg'),(47,12,NULL,'12_dd90bbe5-6991-46ec-948b-640b7d07d73c.jpg'),(48,12,NULL,'12_c9683585-9c04-45b2-aa2f-85511dd1c1a0.jpg'),(49,13,NULL,'13_ed36910a-fb57-453e-ab53-b8a4e3e0f9b7.jpg'),(50,13,NULL,'13_c83845d5-f54c-4ad2-841c-71d39133ba35.jpg'),(51,13,NULL,'13_6ee0f82c-4f27-40cf-a931-a1b99b642297.jpg'),(52,13,NULL,'13_c7e0c24d-3614-4387-9146-23f992edcb44.jpg'),(53,14,NULL,'14_2758b5ea-7890-4912-ad09-df8f8f4365e2.jpg'),(54,14,NULL,'14_15dad0ce-5aae-47b3-af9f-6d0677dd667a.jpg'),(55,14,NULL,'14_a93e623b-a055-4cee-b74a-bb6223dcc2e8.jpg'),(56,14,NULL,'14_9c3ae600-39bf-448a-85f7-c10b6c023c8b.jpg'),(57,15,NULL,'15_7421c0ca-7157-48d1-8dcd-2f7cf843a7a6.jpg'),(58,15,NULL,'15_4ff75255-3266-45cf-994a-190efb55afba.jpg'),(59,15,NULL,'15_4e2661e7-8d68-4e19-803b-45e772d2f465.jpg'),(60,15,NULL,'15_61b62064-b25d-46cd-8a9d-d2d84c196239.jpg'),(61,16,NULL,'16_18a96d92-57fc-445e-bc56-45759a68b819.jpg'),(62,16,NULL,'16_7ba9937d-086e-48cb-8090-676fdee3ed00.jpg'),(63,16,NULL,'16_43189d67-5a98-42a1-ba21-5a841db5ebd4.jpg'),(64,16,NULL,'16_84981014-09f5-4374-a2cd-94312bd59ff2.jpg'),(65,17,NULL,'17_77b78557-5bde-45a1-8fcd-ecc1629ef63a.jpg'),(66,17,NULL,'17_570f24aa-041e-4498-9922-f014ddbae21f.jpg'),(67,17,NULL,'17_395355e8-5f0e-48cb-b7f9-b2ddfcab5f49.jpg'),(68,17,NULL,'17_6722ae3e-93f1-43ea-aecc-8c24f01e0c79.jpg');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas`
--

DROP TABLE IF EXISTS `reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservas` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `fecha_compra` date NOT NULL,
  `fecha_actividad` date NOT NULL,
  `precio_total` decimal(6,2) NOT NULL,
  `cantidad_reservada` tinyint NOT NULL,
  `estado` enum('activa','cancelada') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas`
--

LOCK TABLES `reservas` WRITE;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` VALUES (12,'2021-09-06','2021-09-19',75.00,1,'activa'),(13,'2021-09-06','2021-09-06',50.00,1,'activa'),(14,'2021-09-06','2021-09-25',50.00,1,'activa'),(15,'2021-09-06','2021-09-03',60.00,1,'activa'),(16,'2021-09-06','2021-09-01',60.00,1,'activa'),(17,'2021-09-06','2021-09-05',20.00,1,'activa'),(18,'2021-09-06','2021-09-03',100.00,2,'activa'),(19,'2021-09-06','2021-08-29',120.00,1,'activa'),(20,'2021-09-06','2021-08-22',65.00,1,'activa'),(21,'2021-09-06','2021-07-25',240.00,2,'activa');
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas_experiencias`
--

DROP TABLE IF EXISTS `reservas_experiencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservas_experiencias` (
  `id_reserva` int unsigned NOT NULL,
  `id_experiencia` int unsigned NOT NULL,
  `comentario` text,
  `valoracion` enum('1','2','3','4','5') DEFAULT NULL,
  `estado` enum('pendiente','aprobado') DEFAULT NULL,
  PRIMARY KEY (`id_reserva`,`id_experiencia`),
  KEY `id_experiencia` (`id_experiencia`),
  CONSTRAINT `reservas_experiencias_ibfk_1` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id`),
  CONSTRAINT `reservas_experiencias_ibfk_2` FOREIGN KEY (`id_experiencia`) REFERENCES `experiencias` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas_experiencias`
--

LOCK TABLES `reservas_experiencias` WRITE;
/*!40000 ALTER TABLE `reservas_experiencias` DISABLE KEYS */;
INSERT INTO `reservas_experiencias` VALUES (12,8,'Merece mucho la pena, experiencia increíble.','5','aprobado'),(13,4,'Todo genial.','5','aprobado'),(14,6,'Todo perfecto, muy atentos.','5','aprobado'),(15,16,'Las motos tenían una gran potencia, una experiencia inolvidable. Repetiré sin duda.','5','aprobado'),(16,17,'El instructor muy atento, estuvo genial. Volveré seguro.\n','5','aprobado'),(17,5,'Merece mucho la pena, experiencia increíble.','5','aprobado'),(18,4,'Unas olas increíbles, volveré! Fantástico!','5','aprobado'),(19,9,'Experiencia inolvidable. ','5','pendiente'),(20,10,'Experiencia maravillosa.','5','pendiente'),(21,12,'El spa es increíble, relajación total. Repetiré seguro.','5','pendiente');
/*!40000 ALTER TABLE `reservas_experiencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `dni` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `rol` enum('administrador','registrado') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `telefono` (`telefono`),
  UNIQUE KEY `dni` (`dni`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (2,'Viviane','Aragao Gomes','vivins_02@hotmail.com','$2a$10$pmhX49WnEXL2/2RhKHDt2upfAg3lkIAydIeDbvIHgVt6a55wU/0TS','666 666 666','Calle Vigo, 1 - Vigo','Y2743800Y','1983-11-18','2_c2150a44-2e24-4a07-a32c-91d9ae307cd7.jpg','administrador'),(17,'Ana','Alvarez Fernandez','ana@gmail.com','$2a$10$SIgD.nnZrftiCI8L8BeqnOCbNYpKsI4701vcCZ7EWCxo.ae4cfwK.','987384576','Calle Urzáiz, 12 - Vigo','32743800L','1989-10-15','17_5fbdee9c-3636-4ad7-b455-5cc9d6df05ca.jpg','registrado'),(18,'Carlos','Pousa Marcote','capouma@gmail.com','$2a$10$z0IBLK0vFVflGOCploiWyenTHypUXZOXSZVjRkAuXFUnMrE2ZEN.O','670462461','Rua cidade de Vigo','77777777C','1982-03-20','18_857af209-2af5-47b4-bd11-ccf1add96853.jpg','administrador'),(19,'Elsa','Sio Ramirez','elsasioramirez@gmail.com','$2a$10$rlRCt21iPZEgcCUnd6LVyuTgLcDCnvnj3AZqf3QlZ3sWf1qt.Kpv.','664716662','San Eleuterio, 27 Tameiga Mos','35571371A','1991-04-06','19_38353a09-3946-4fca-86e3-b147624e5ca4.jpg','administrador');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_reservas`
--

DROP TABLE IF EXISTS `usuarios_reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_reservas` (
  `id_usuario` int unsigned NOT NULL,
  `id_reserva` int unsigned NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_reserva`),
  KEY `id_reserva` (`id_reserva`),
  CONSTRAINT `usuarios_reservas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `usuarios_reservas_ibfk_2` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_reservas`
--

LOCK TABLES `usuarios_reservas` WRITE;
/*!40000 ALTER TABLE `usuarios_reservas` DISABLE KEYS */;
INSERT INTO `usuarios_reservas` VALUES (19,12),(19,13),(19,14),(19,15),(19,16),(19,17),(17,18),(18,19),(18,20),(18,21);
/*!40000 ALTER TABLE `usuarios_reservas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-06 18:26:00
