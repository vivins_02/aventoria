# Aventoria

Web de venta de experiencias diferentes, donde los usuarios pueden encontrar actividades diferentes, filtrando por precios, ciudad y tipo de actividad.

Backend
API creada en Node.js con Express y algunas de sus dependencias, utilizando como base de datos Mysql.

Frontend
Creado con Create React App

Ejecución
Para iniciar tanto el backend como el frontend ejecutamos en la Terminal el comando [npm install] en cada una de las carpetas. Este comando instalará todas las dependencias necesarias para el funcionamento del proyecto.
